
#include "gph1algo.h"

typedef struct Gph1topostruct_t
{
  Gph1cont * cont;
  Idx1range * range;
  bool cycle;
}
Gph1topostruct;

static void gph1cont_topo_sort_close(void * userstruct, uint32_t vtx)
{
  Gph1topostruct * topo = (Gph1topostruct *)userstruct;
  Idx1it it;
//printf("    close %c\n", (char)vtx);
  gph1cont_del_dir1_vtx(topo->cont, vtx);
  idx32queue_push_front(topo->range, vtx);
}

static void gph1cont_topo_sort_cycle(void * userstruct, uint32_t vtx)
{
  Gph1topostruct * topo = (Gph1topostruct *)userstruct;
//printf("    cycle %c\n", (char)vtx);
  topo->cycle = true;
}

bool gph1cont_topo_sort(Gph1cont * cont, Idx1range * out_vtxlist)
{
  Gph1cont copy = gph1cont_copy(cont);
  Hsh1cont vtxcolor = hsh32map32_new();
  Gph1topostruct topo = { .cont = &copy, .range = out_vtxlist, .cycle = false };
  *out_vtxlist = idx1range_zero();
  do
  {
    Vtx1it vit;
    Dfs1it dit;
    Dfs1it begin = dfs1it_zero();
    for (vit = vtx1it_begin(&copy); vtx1it_valid(&vit); vtx1it_increment(&vit))
    {
      uint32_t vtx = vtx1it_getvtx(&vit);
      uint32_t color;
      if (hsh32map32_get(&vtxcolor, vtx, &color))
      {
//printf("    skip %c, color = %d\n", (char)vtx, color);
        continue;
      }
//printf("    start %c, color = %d\n", (char)vtx, color);
      begin = dfs1it_begin_vtx(&copy, vtx, &vtxcolor);
      break;
    }
    if (dfs1it_valid(&begin))
    {
      begin.close = gph1cont_topo_sort_close;
      begin.cycle = gph1cont_topo_sort_cycle;
      begin.userstruct = &topo;
      for (dit = begin; dfs1it_valid(&dit); dfs1it_increment(&dit))
      {
      }
      if (!topo.cycle)
        continue;
    }
    break;
  }
  while (1);
  hsh1cont_release(&vtxcolor);
  gph1cont_release(&copy);
  return !(topo.cycle || idx1queue_empty(out_vtxlist));
}
