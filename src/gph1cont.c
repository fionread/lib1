
#include "gph1cont.h"
#include "idx1internal.h"

#include <stdio.h> //TMP!!!

#include <assert.h>

const uint32_t gph1vnoway_flag = 1U << 31;
const uint32_t gph1vindex_mask = (1U << 31) - 1;


//"generic" graph methods family

void gph1cont_clear(Gph1cont * cont)
{
  hsh1cont_clear(&cont->adjlist);
}

void gph1cont_release(Gph1cont * cont)
{
  hsh1cont_release(&cont->adjlist);
}

Gph1cont gph1cont_copy(Gph1cont * from)
{
  return (Gph1cont){ .adjlist = hsh1cont_copy(&from->adjlist) };
}

//vertex iterator methods family

static Idx1cont vtx1it_start_impl(Hsh1cont * adjlist, uint32_t start)
{
  Idx1it it = idx1it_zero();
  uint32_t n;
  for (n = start; n < adjlist->num_buckets; ++n)
  {
    if (adjlist->bucket[n].base_idx & 0xffffff)
    {
      it = idx1it_begin(&adjlist->bucket[n]);
      break;
    }
  }
  return (Idx1cont){ .base_idx = n, .end = it };
}

Vtx1it vtx1it_begin(Gph1cont * cont)
{
  Hsh1cont * adjlist = &cont->adjlist;
  return (Vtx1it){ .bucket_it = vtx1it_start_impl(adjlist, 0), .adjlist = adjlist };
}

bool vtx1it_valid(const Vtx1it * it)
{
  return it->bucket_it.base_idx < it->adjlist->num_buckets && idx1it_valid(&it->bucket_it.end);
}

void vtx1it_increment(Vtx1it * it)
{
  idx1it_increment(&it->bucket_it.end);
  if (idx1it_valid(&it->bucket_it.end))
    ;
  else
    it->bucket_it = vtx1it_start_impl(it->adjlist, ++it->bucket_it.base_idx);
}

uint32_t vtx1it_getvtx(const Vtx1it * it)
{
  Idx1range val128 = idx128it_getval(&it->bucket_it.end);
  return val128.begin.chunk;
}

Idx1cont vtx1it_adjlist(const Vtx1it * it)
{
  Idx1range val128 = idx128it_getval(&it->bucket_it.end);
  return (Idx1cont){ .base_idx = val128.begin.count, .end = val128.end };
}

//deep-first-search iterator methods family

Dfs1it dfs1it_zero()
{
  return (Dfs1it){
    .adjstack = idx1cont_zero(),
    .adjlist = NULL,
    .vtxcolor = NULL,
    .close = NULL,
    .cycle = NULL,
    .userstruct = NULL
  };
}

Dfs1it dfs1it_begin_vtx(Gph1cont * cont, uint32_t vtx, Hsh1cont * vtxcolor)
{
  Dfs1it it = {
    .adjstack = idx1cont_zero(),
    .adjlist = &cont->adjlist,
    .vtxcolor = vtxcolor,
    .close = NULL,
    .cycle = NULL,
    .userstruct = NULL
  };
  Idx1cont adjlist;
  hsh32map96_get(&cont->adjlist, vtx, &adjlist);
  idx128stack_push(&it.adjstack, (Idx1range){
    .begin = { .chunk = vtx, .count = -1 }, .end = idx1it_begin(&adjlist) });
  return it;
}

bool dfs1it_valid(const Dfs1it * it)
{
  return !idx1stack_empty(&it->adjstack);
}

void dfs1it_increment(Dfs1it * it)
{
  while (!idx1stack_empty(&it->adjstack))
  {
    uint64_t val64;
    Idx1range adjit;
    for (adjit = idx128stack_top(&it->adjstack); idx1it_valid(&adjit.end); idx1it_increment(&adjit.end))
    {
      val64 = idx64it_getval(&adjit.end);
      if (((val64 >> 32) & gph1vnoway_flag) || ((int32_t)val64 == adjit.begin.count))
        continue;
      break;
    }
//printf("    (input) vtx = %c, parent = %c\n", (char)adjit.begin.chunk, (char)adjit.begin.count);
    if (idx1it_valid(&adjit.end))
    {
      Idx1cont adjlist;
      uint32_t vtx = (uint32_t)val64;
      Vtx1color color;
      idx1it_increment(&adjit.end);
      idx128stack_alter(&it->adjstack, adjit);
      hsh32map96_get(it->adjlist, vtx, &adjlist);
      hsh32map32_set(it->vtxcolor, adjit.begin.chunk, Vtx1color_grey);
      adjit = (Idx1range){
        .begin = { .chunk = vtx, .count = adjit.begin.chunk }, .end = idx1it_begin(&adjlist) };
      hsh32map32_get(it->vtxcolor, adjit.begin.chunk, &color);
//printf("    (child) vtx = %c, color = %d\n", (char)adjit.begin.chunk, color);
      if (color == Vtx1color_white)
      {
        idx128stack_push(&it->adjstack, adjit);
        break;
      }
      if (color == Vtx1color_grey)
      {
        if (it->cycle != NULL)
          it->cycle(it->userstruct, adjit.begin.chunk);
      }
      continue;
    }
    hsh32map32_set(it->vtxcolor, adjit.begin.chunk, Vtx1color_black);
    if (it->close != NULL)
      it->close(it->userstruct, adjit.begin.chunk);
//printf("    (close) vtx = %c, color = 2\n", (char)adjit.begin.chunk);
    idx1stack_pop(&it->adjstack);
  }
}

uint32_t dfs1it_getvtx(const Dfs1it * it)
{
  Idx1range adjit = idx128stack_top(&it->adjstack);
  return adjit.begin.chunk;
}

Idx1cont dfs1it_adjlist(const Dfs1it * it)
{
  Idx1cont res;
  hsh32map96_get(it->adjlist, dfs1it_getvtx(it), &res);
  return res;
}

void dfs1it_finish(Dfs1it * it)
{
  idx1cont_release(&it->adjstack);
}

//breadth-first search iterator methods family

Bfs1it bfs1it_zero()
{
  return (Bfs1it){ .adjstack = idx1range_zero(), .adjlist = NULL, .vtxcolor = NULL };
}

Bfs1it bfs1it_begin_vtx(Gph1cont * cont, uint32_t vtx, Hsh1cont * vtxcolor)
{
  Bfs1it it = { .adjstack = idx1range_zero(), .adjlist = &cont->adjlist, .vtxcolor = vtxcolor };
  Idx1cont adjlist;
  hsh32map96_get(&cont->adjlist, vtx, &adjlist);
  idx128queue_push_back(&it.adjstack, (Idx1range){
    .begin = { .chunk = vtx, .count = -1 }, .end = idx1it_begin(&adjlist) });
  return it;
}

bool bfs1it_valid(const Bfs1it * it)
{
  return !idx1queue_empty(&it->adjstack);
}

void bfs1it_increment(Bfs1it * it)
{
  while (!idx1queue_empty(&it->adjstack))
  {
    uint64_t val64;
    Idx1range adjit;
    for (adjit = idx128queue_front(&it->adjstack); idx1it_valid(&adjit.end); idx1it_increment(&adjit.end))
    {
      val64 = idx64it_getval(&adjit.end);
      if (((val64 >> 32) & gph1vnoway_flag) || ((int32_t)val64 == adjit.begin.count))
        continue;
      break;
    }
//printf("    (input) vtx = %c, parent = %c\n", (char)adjit.begin.chunk, (char)adjit.begin.count);
    if (idx1it_valid(&adjit.end))
    {
      Idx1cont adjlist;
      uint32_t vtx = (uint32_t)val64;
      Vtx1color color;
      idx1it_increment(&adjit.end);
      idx128queue_alter_front(&it->adjstack, adjit);
      hsh32map96_get(it->adjlist, vtx, &adjlist);
      adjit = (Idx1range){
        .begin = { .chunk = vtx, .count = adjit.begin.chunk }, .end = idx1it_begin(&adjlist) };
      hsh32map32_get(it->vtxcolor, adjit.begin.chunk, &color);
//printf("    (child) vtx = %c, color = %d\n", (char)adjit.begin.chunk, color);
      if (color == Vtx1color_white)
      {
        idx128queue_push_back(&it->adjstack, adjit);
        hsh32map32_set(it->vtxcolor, adjit.begin.chunk, Vtx1color_grey);
        break;
      }
    }
    hsh32map32_set(it->vtxcolor, adjit.begin.chunk, Vtx1color_black);
    idx1queue_pop_front(&it->adjstack);
  }
}

uint32_t bfs1it_getvtx(const Bfs1it * it)
{
  Idx1range adjit = idx128queue_back(&it->adjstack);
  return adjit.begin.chunk;
}

Idx1cont bfs1it_adjlist(const Bfs1it * it)
{
  Idx1cont res;
  hsh32map96_get(it->adjlist, bfs1it_getvtx(it), &res);
  return res;
}

void bfs1it_finish(Bfs1it * it)
{
  idx1queue_release(&it->adjstack);
}

//vertex cross indexing helpers

static void gph1dir2_untangle_cross_indexing(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  Idx1cont * ptr0, * ptr1;
  Idx1cont adj0, adj1;
  int cnt0, cnt1;
  uint32_t cmp0;
  Idx1it it;
  hsh32map96_get(&cont->adjlist, vtx0, &adj0);
  hsh32map96_get(&cont->adjlist, vtx1, &adj1);
  assert(adj0.end.count);
  assert(adj1.end.count);
  ptr0 = adj0.end.count < adj1.end.count ? &adj0 : &adj1;
  ptr1 = adj0.end.count < adj1.end.count ? &adj1 : &adj0;
  assert(ptr0 != ptr1);
  cmp0 = adj0.end.count < adj1.end.count ? vtx1 : vtx0;
  cnt0 = 0;
  for (it = idx1it_begin(ptr0); idx1it_valid(&it); idx1it_increment(&it))
  {
    uint64_t val64 = idx64it_getval(&it);
    cnt1 = (val64 >> 32) & gph1vindex_mask;
    if ((uint32_t)val64 == cmp0)
    {
      uint64_t top0 = idx64stack_top(ptr0);
      uint64_t top1 = idx64stack_top(ptr1);
      int idx0 = (top0 >> 32) & gph1vindex_mask;
      int idx1 = (top1 >> 32) & gph1vindex_mask;
      Idx1cont ext0, ext1;
      Idx1it it0, it1, itr;
      uint64_t val0, val1;
      uint64_t flag0, flag1;

      hsh32map96_get(&cont->adjlist, (uint32_t)top0, &ext0);
      assert(ext0.end.count);
      assert(idx0 < ext0.end.count);
      it0 = idx64it_cont_idx(&ext0, idx0);
      assert(idx1it_valid(&it0));
      val0 = idx64it_getval(&it0);
      flag0 = (val0 >> 32) & gph1vnoway_flag;
      idx64it_setval(&it0, (uint32_t)val0 | (((uint64_t)cnt0 | flag0) << 32));
        
      hsh32map96_get(&cont->adjlist, (uint32_t)top1, &ext1);
      assert(ext1.end.count);
      assert(idx1 < ext1.end.count);
      it1 = idx64it_cont_idx(&ext1, idx1);
      assert(idx1it_valid(&it1));
      val1 = idx64it_getval(&it1);
      flag1 = (val1 >> 32) & gph1vnoway_flag;
      idx64it_setval(&it1, (uint32_t)val1 | (((uint64_t)cnt1 | flag1) << 32));

      assert(cnt1 < ptr1->end.count);
      itr = idx64it_cont_idx(ptr1, cnt1);
      assert(idx1it_valid(&itr));
      assert(idx64it_getval(&itr));
      idx64cont_remove(ptr0, &it);
      idx64cont_remove(ptr1, &itr);
      break;
    }
    ++cnt0;
  }
  hsh32map96_set(&cont->adjlist, vtx0, adj0);
  hsh32map96_set(&cont->adjlist, vtx1, adj1);
  if (adj0.base_idx & 0xffffff)
    ;
  else
    hsh32map96_del(&cont->adjlist, vtx0);
  if (adj1.base_idx & 0xffffff)
    ;
  else
    hsh32map96_del(&cont->adjlist, vtx1);
}

static void gph1dir2_entangle_cross_indexing(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1, bool dir1)
{
  Idx1cont adj0, adj1;
  int cnt0, cnt1;
  uint32_t nbr0, nbr1;
  hsh32map96_get(&cont->adjlist, vtx0, &adj0);
  hsh32map96_get(&cont->adjlist, vtx1, &adj1);
  cnt0 = adj0.end.count;
  cnt1 = adj1.end.count;
  idx64cont_append(&adj0, vtx1 | ((uint64_t)cnt1 << 32));
  idx64cont_append(&adj1, vtx0 | (((uint64_t)cnt0 | (dir1 ? gph1vnoway_flag : 0)) << 32));
  hsh32map96_set(&cont->adjlist, vtx0, adj0);
  hsh32map96_set(&cont->adjlist, vtx1, adj1);
}

//Gph1cont methods family

Gph1cont gph1cont_new()
{
  return (Gph1cont){ .adjlist = hsh32map96_new(true/*clean values*/) };
}

static bool gph1cont_has_impl(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1, bool dir1)
{
  Idx1cont adj0;
  if (hsh32map96_get(&cont->adjlist, vtx0, &adj0))
  {
    Idx1it it0;
    for (it0 = idx1it_begin(&adj0); idx1it_valid(&it0); idx1it_increment(&it0))
    {
      uint64_t val64 = idx64it_getval(&it0);
      if ((uint32_t)val64 == vtx1)
      {
        uint32_t cnt1 = val64 >> 32;
        bool noway = cnt1 & gph1vnoway_flag;
        if (!(noway && dir1))
          return true;
        break;
      }
    }
  }
  return false;
}

static void gph1cont_dir_mark_noway(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  Idx1cont adj0;
  if (hsh32map96_get(&cont->adjlist, vtx0, &adj0))
  {
    Idx1it it0;
    for (it0 = idx1it_begin(&adj0); idx1it_valid(&it0); idx1it_increment(&it0))
    {
      uint64_t val64 = idx64it_getval(&it0);
      if ((uint32_t)val64 == vtx1)
      {
        val64 |= (uint64_t)gph1vnoway_flag << 32;
        idx64it_setval(&it0, val64);
        break;
      }
    }
  }
}

static bool gph1cont_add_impl(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1, bool dir1)
{
  if (!gph1cont_has_dir2(cont, vtx0, vtx1))
  {
    gph1dir2_entangle_cross_indexing(cont, vtx0, vtx1, dir1);
    return true;
  }
  return false;
}

bool gph1cont_add_dir1(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  return gph1cont_add_impl(cont, vtx0, vtx1, true/*dir1*/);
}

bool gph1cont_add_dir2(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  return gph1cont_add_impl(cont, vtx0, vtx1, false/*dir1*/);
}

bool gph1cont_has_dir1(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  return gph1cont_has_impl(cont, vtx0, vtx1, true/*dir1*/);
}

bool gph1cont_has_dir2(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  return gph1cont_has_impl(cont, vtx0, vtx1, false/*dir1*/);
}

void gph1cont_del_dir1(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  if (gph1cont_has_dir1(cont, vtx1, vtx0))
    gph1cont_dir_mark_noway(cont, vtx0, vtx1);
  else
    gph1dir2_untangle_cross_indexing(cont, vtx0, vtx1);
}

void gph1cont_del_dir2(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1)
{
  gph1dir2_untangle_cross_indexing(cont, vtx0, vtx1);
}

bool gph1cont_del_dir2_vtx(Gph1cont * cont, uint32_t vtx)
{
  Idx1cont adj0;
  if (hsh32map96_get(&cont->adjlist, vtx, &adj0))
  {
    Idx1cont adj1 = idx1cont_copy(&adj0);
    Idx1it it;
    for (it = idx1it_begin(&adj1); idx1it_valid(&it); idx1it_increment(&it))
      gph1cont_del_dir2(cont, vtx, (uint32_t)idx64it_getval(&it));
    idx1cont_release(&adj1);
    return true;
  }
  return false;
}

bool gph1cont_del_dir1_vtx(Gph1cont * cont, uint32_t vtx)
{
  Idx1cont adj0;
  if (hsh32map96_get(&cont->adjlist, vtx, &adj0))
  {
    Idx1cont adj1 = idx1cont_copy(&adj0);
    Idx1it it;
    for (it = idx1it_begin(&adj1); idx1it_valid(&it); idx1it_increment(&it))
    {
      uint64_t val64 = idx64it_getval(&it);
      if ((val64 >> 32) & gph1vnoway_flag)
        ;
      else
        gph1cont_del_dir1(cont, vtx, (uint32_t)val64);
    }
    idx1cont_release(&adj1);
    return true;
  }
  return false;
}
