
#pragma once

#include "hsh1cont.h"

typedef struct Gph1cont_t
{
  Hsh1cont adjlist;
}
Gph1cont;

typedef enum Vtx1color_en
{
  Vtx1color_white = 0,
  Vtx1color_grey,
  Vtx1color_black
}
Vtx1color;

typedef struct Vtx1it_t
{
  Idx1cont bucket_it;
  Hsh1cont * adjlist;
}
Vtx1it;

typedef void (*dfs1visitor)(void * userstruct, uint32_t vtx);

typedef struct Dfs1it_t
{
  Idx1cont adjstack;
  Hsh1cont * adjlist;
  Hsh1cont * vtxcolor;
  dfs1visitor close;
  dfs1visitor cycle;
  void * userstruct;
}
Dfs1it;

typedef struct Bfs1it_t
{
  Idx1range adjstack;
  Hsh1cont * adjlist;
  Hsh1cont * vtxcolor;
}
Bfs1it;


extern const uint32_t gph1vnoway_flag; //= 1U << 31;
extern const uint32_t gph1vindex_mask; //(1U << 31) - 1;


//delete "generic" graph
void gph1cont_release(Gph1cont * cont);

//remove all data
void gph1cont_clear(Gph1cont * cont);

//"generic" graph deep copy
Gph1cont gph1cont_copy(Gph1cont * from);

//vertex iterator methods family
Vtx1it vtx1it_begin(Gph1cont * cont);
bool vtx1it_valid(const Vtx1it * it);
void vtx1it_increment(Vtx1it * it);
uint32_t vtx1it_getvtx(const Vtx1it * it);
Idx1cont vtx1it_adjlist(const Vtx1it * it);

//deep-first search iterator methods family
Dfs1it dfs1it_zero();
Dfs1it dfs1it_begin_vtx(Gph1cont * cont, uint32_t vtx, Hsh1cont * vtxcolor);
bool dfs1it_valid(const Dfs1it * it);
void dfs1it_increment(Dfs1it * it);
uint32_t dfs1it_getvtx(const Dfs1it * it);
Idx1cont dfs1it_adjlist(const Dfs1it * it);
void dfs1it_finish(Dfs1it * it);

//breadth-first search iterator methods family
Bfs1it bfs1it_zero();
Bfs1it bfs1it_begin_vtx(Gph1cont * cont, uint32_t vtx, Hsh1cont * vtxcolor);
bool bfs1it_valid(const Bfs1it * it);
void bfs1it_increment(Bfs1it * it);
uint32_t bfs1it_getvtx(const Bfs1it * it);
Idx1cont bfs1it_adjlist(const Bfs1it * it);
void bfs1it_finish(Bfs1it * it);

//Gph1cont methods family
Gph1cont gph1cont_new();

bool gph1cont_add_dir1(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1);
bool gph1cont_has_dir1(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1);
void gph1cont_del_dir1(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1);
bool gph1cont_del_dir1_vtx(Gph1cont * cont, uint32_t vtx);

bool gph1cont_add_dir2(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1);
bool gph1cont_has_dir2(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1);
void gph1cont_del_dir2(Gph1cont * cont, uint32_t vtx0, uint32_t vtx1);
bool gph1cont_del_dir2_vtx(Gph1cont * cont, uint32_t vtx);
