
#include "hsh1cont.h"
#include "idx1internal.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>


#define HSH32BUCKET_MAX (1 << 24)

static uint8_t hsh1cont_N = 2;

enum Hsh1bucket_en
{
  Hsh1bucket_lim32  = /*24*/12,
  Hsh1bucket_lim64  = /*12*/6,
  Hsh1bucket_lim96  = /* 8*/4,
  Hsh1bucket_lim128 = /* 6*/3,
  Hsh1bucket_lim160 = /* 5*/3,
  Hsh1bucket_lim192 = /* 4*/2
};

typedef enum Hsh1valtype_en
{
  Hsh1valtype_plain,
  Hsh1valtype_cont,
  Hsh1valtype_queue
}
Hsh1valtype;

static void hsh1cont_reserve(Hsh1cont * cont, uint32_t count)
{
  Idx1cont * ptr = NULL;
  assert(count > 0);
  assert(count <= HSH32BUCKET_MAX);
  ptr = (Idx1cont *)malloc(sizeof(Idx1cont) * count);
  assert(ptr);
  if (cont->bucket != NULL)
  {
    uint32_t old_count = count >> 1;
    memcpy(ptr, cont->bucket, sizeof(Idx1cont) * old_count);
    memset((char *)ptr + sizeof(Idx1cont) * old_count, 0, (count - old_count) * sizeof(Idx1cont));
    free(cont->bucket);
  }  
  cont->bucket = ptr;
}

typedef void (*hsh1cont_rehash)(Hsh1cont * cont);

static void hsh1cont_add_bucket(Hsh1cont * cont, hsh1cont_rehash rehash)
{
  uint32_t curr_count = hsh1cont_N * (1 << cont->L);
  uint32_t next_count = curr_count * 2;
  if (cont->num_buckets == curr_count)
    hsh1cont_reserve(cont, next_count);
  rehash(cont);
  ++cont->S;
  ++cont->num_buckets;
  if (cont->num_buckets == next_count)
  {
    cont->S = 0;
    ++cont->L;
  }
}

static void hsh1cont_add_check(Hsh1cont * cont, uint32_t bucket_no, uint32_t lim, hsh1cont_rehash rehash)
{
  if (cont->bucket[bucket_no].end.count < lim)
    ;
  else
    hsh1cont_add_bucket(cont, rehash);
}

static Hsh1cont hsh1cont_new(uint32_t bits, Hsh1valtype type)
{
  uint32_t n;
  Hsh1cont res;
  res.bucket = NULL;
  res.num_buckets = hsh1cont_N;
  res.S = 0;
  res.L = 0;
  res.C = type == Hsh1valtype_cont;
  res.Q = type == Hsh1valtype_queue;
  hsh1cont_reserve(&res, res.num_buckets);
  for (n = 0; n < res.num_buckets; ++n)
    res.bucket[n] = (Idx1cont){ .base_idx = bits << 24, .end = idx1it_zero() };
  return res;
}

Hsh1cont hsh1cont_copy(Hsh1cont * from)
{
  uint32_t cap = hsh1cont_N * (1 << from->L);
  uint32_t n;
  Hsh1cont res;
  res.bucket = NULL;
  res.num_buckets = from->num_buckets;
  res.S = from->S;
  res.L = from->L;
  res.C = from->C;
  res.Q = from->Q;
  hsh1cont_reserve(&res, cap < from->num_buckets ? cap * 2 : cap);
  for (n = 0; n < from->num_buckets; ++n)
  {
    if (from->bucket[n].base_idx & 0xffffff)
      res.bucket[n] = idx1cont_copy(&from->bucket[n]);
    else
      res.bucket[n] = from->bucket[n];
  }
  if (from->C)
  {
    switch (idx1cont_get_bits(&from->bucket[0]))
    {
      case 128:
        for (n = 0; n < from->num_buckets; ++n)
        {
          if (from->bucket[n].base_idx & 0xffffff)
          {
            Idx1it itr = idx1it_begin(&from->bucket[n]);
            Idx1it itw = idx1it_begin(&res.bucket[n]);
            for ( ; idx1it_valid(&itr); idx1it_increment(&itr), idx1it_increment(&itw))
            {
              Idx1range range = idx128it_getval(&itr);
              Idx1cont cont = { .base_idx = range.begin.count, .end = range.end };
              cont = idx1cont_copy(&cont);
              range.begin.count = cont.base_idx;
              range.end = cont.end;
              idx128it_setval(&itw, range);
            }
          }
        }
        break;
      case 160:
        for (n = 0; n < from->num_buckets; ++n)
        {
          if (from->bucket[n].base_idx & 0xffffff)
          {
            Idx1it itr = idx1it_begin(&from->bucket[n]);
            Idx1it itw = idx1it_begin(&res.bucket[n]);
            for ( ; idx1it_valid(&itr); idx1it_increment(&itr), idx1it_increment(&itw))
            {
              Idx1nonsense1 nons = idx160it_getval(&itr);
              nons.cont = idx1cont_copy(&nons.cont);
              idx160it_setval(&itw, nons);
            }
          }
        }
        break;
      default:
        assert(false);
    }
  }
  if (from->Q)
  {
    switch (idx1cont_get_bits(&from->bucket[0]))
    {
      case 160:
        for (n = 0; n < from->num_buckets; ++n)
        {
          if (from->bucket[n].base_idx & 0xffffff)
          {
            Idx1it itr = idx1it_begin(&from->bucket[n]);
            Idx1it itw = idx1it_begin(&res.bucket[n]);
            for ( ; idx1it_valid(&itr); idx1it_increment(&itr), idx1it_increment(&itw))
            {
              Idx1nonsense1 nons = idx160it_getval(&itr);
              Idx1range range = { .begin = nons.cont.end, .end = nons.end };
              range = idx1range_copy(&range);
              nons.cont.end = range.begin;
              nons.end = range.end;
              idx160it_setval(&itw, nons);
            }
          }
        }
        break;
      case 192:
        for (n = 0; n < from->num_buckets; ++n)
        {
          if (from->bucket[n].base_idx & 0xffffff)
          {
            Idx1it itr = idx1it_begin(&from->bucket[n]);
            Idx1it itw = idx1it_begin(&res.bucket[n]);
            for ( ; idx1it_valid(&itr); idx1it_increment(&itr), idx1it_increment(&itw))
            {
              Idx1nonsense2 nons = idx192it_getval(&itr);
              nons.range = idx1range_copy(&nons.range);
              idx192it_setval(&itw, nons);
            }
          }
        }
        break;
      default:
        assert(false);
    }
  }
  return res;
}

void hsh1cont_clear(Hsh1cont * cont)
{
  uint32_t n;
  if (cont->C)
  {
    switch (idx1cont_get_bits(&cont->bucket[0]))
    {
      case 128:
        for (n = 0; n < cont->num_buckets; ++n)
        {
          Idx1it it;
          if (cont->bucket[n].base_idx & 0xffffff)
          {
            for (it = idx1it_begin(&cont->bucket[n]); idx1it_valid(&it); idx1it_increment(&it))
            {
              Idx1range range = idx128it_getval(&it);
              Idx1cont cont = { .base_idx = range.begin.count, .end = range.end };
              idx1cont_release(&cont);
            }
          }
        }
        break;
      case 160:
        for (n = 0; n < cont->num_buckets; ++n)
        {
          Idx1it it;
          if (cont->bucket[n].base_idx & 0xffffff)
          {
            for (it = idx1it_begin(&cont->bucket[n]); idx1it_valid(&it); idx1it_increment(&it))
            {
              Idx1nonsense1 nons = idx160it_getval(&it);
              idx1cont_release(&nons.cont);
            }
          }
        }
        break;
      default:
        assert(false);
    }
  }
  if (cont->Q)
  {
    switch (idx1cont_get_bits(&cont->bucket[0]))
    {
      case 160:
        for (n = 0; n < cont->num_buckets; ++n)
        {
          Idx1it it;
          if (cont->bucket[n].base_idx & 0xffffff)
          {
            for (it = idx1it_begin(&cont->bucket[n]); idx1it_valid(&it); idx1it_increment(&it))
            {
              Idx1nonsense1 nons = idx160it_getval(&it);
              Idx1range range = { .begin = nons.cont.end, .end = nons.end };
              idx1queue_release(&range);
            }
          }
        }
        break;
      case 192:
        for (n = 0; n < cont->num_buckets; ++n)
        {
          Idx1it it;
          if (cont->bucket[n].base_idx & 0xffffff)
          {
            for (it = idx1it_begin(&cont->bucket[n]); idx1it_valid(&it); idx1it_increment(&it))
            {
              Idx1nonsense2 nons = idx192it_getval(&it);
              idx1queue_release(&nons.range);
            }
          }
        }
        break;
      default:
        assert(false);
    }
  }
  for (n = 0; n < cont->num_buckets; ++n)
    idx1cont_release(&cont->bucket[n]);
}

void hsh1cont_release(Hsh1cont * cont)
{
  hsh1cont_clear(cont);
  free(cont->bucket);
  cont->bucket = NULL;
}

//hsh32set methods family

static uint32_t hsh32key_to_bucket_no(Hsh1cont * cont, uint32_t key)
{
  uint32_t curr_count = hsh1cont_N * (1 << cont->L);
  uint32_t curr_mask = curr_count - 1;
  uint32_t bucket_no = key & curr_mask; 
  if (bucket_no < cont->S)
  {
    uint32_t next_count = curr_count * 2;
    uint32_t next_mask = next_count - 1;
    bucket_no = key & next_mask;
  }
  return bucket_no;
}

static void hsh32set_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it32 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it32))
  {
    uint32_t val32 = idx32it_getval(&it32);
    uint32_t bucket_no = val32 & mask;
    if (bucket_no != cont->S)
    {
      idx32cont_remove(&cont->bucket[cont->S], &it32);
      idx32cont_append(&cont->bucket[bucket_no], val32);
      continue;
    }
    idx1it_increment(&it32);
  }
}

static bool hsh32set_impl(Hsh1cont * cont, Idx1it * it32, uint32_t key, uint32_t bucket_no)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it32 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it32); idx1it_increment(it32))
    {
      if (idx32it_getval(it32) != key)
        continue;
      return true;
    }
  }
  return false;
}

Hsh1cont hsh32set_new()
{
  return hsh1cont_new(32, Hsh1valtype_plain);
}

bool hsh32set_add(Hsh1cont * cont, uint32_t key)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it32;
  if (!hsh32set_impl(cont, &it32, key, bucket_no))
  {
    idx32cont_append(&cont->bucket[bucket_no], key);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim32, hsh32set_rehash);
    return true;
  }
  return false;
}

bool hsh32set_has(Hsh1cont * cont, uint32_t key)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it32;
  return hsh32set_impl(cont, &it32, key, bucket_no);
}

bool hsh32set_del(Hsh1cont * cont, uint32_t key)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it32;
  if (hsh32set_impl(cont, &it32, key, bucket_no))
  {
    idx32cont_remove(&cont->bucket[bucket_no], &it32);
    return true;
  }
  return false;
}

//Hsh64set methods family

static uint32_t hsh64key_to_bucket_no(Hsh1cont * cont, uint64_t key)
{
  uint32_t key32 = (key >> 32) ^ key;
  return hsh32key_to_bucket_no(cont, key32);
}

static void hsh64set_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it64 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it64))
  {
    uint64_t val64 = idx64it_getval(&it64);
    uint32_t key32 = (val64 >> 32) ^ val64;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx64cont_remove(&cont->bucket[cont->S], &it64);
      idx64cont_append(&cont->bucket[bucket_no], val64);
      continue;
    }
    idx1it_increment(&it64);
  }
}

static bool hsh64set_impl(Hsh1cont * cont, Idx1it * it64, uint64_t key, uint32_t bucket_no)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it64 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it64); idx1it_increment(it64))
    {
      if (idx64it_getval(it64) != key)
        continue;
      return true;
    }
  }
  return false;
}

Hsh1cont hsh64set_new()
{
  return hsh1cont_new(64, Hsh1valtype_plain);
}

bool hsh64set_add(Hsh1cont * cont, uint64_t key)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it64;
  if (!hsh64set_impl(cont, &it64, key, bucket_no))
  {
    idx64cont_append(&cont->bucket[bucket_no], key);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim64, hsh64set_rehash);
    return true;
  }
  return false;
}

bool hsh64set_has(Hsh1cont * cont, uint64_t key)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it64;
  return hsh64set_impl(cont, &it64, key, bucket_no);
}

bool hsh64set_del(Hsh1cont * cont, uint64_t key)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it64;
  if (hsh64set_impl(cont, &it64, key, bucket_no))
  {
    idx64cont_remove(&cont->bucket[bucket_no], &it64);
    return true;
  }
  return false;
}

//hsh32map32 methods family

static void hsh32map32_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it64 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it64))
  {
    uint64_t val64 = idx64it_getval(&it64);
    uint32_t key32 = (uint32_t)val64;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx64cont_remove(&cont->bucket[cont->S], &it64);
      idx64cont_append(&cont->bucket[bucket_no], val64);
      continue;
    }
    idx1it_increment(&it64);
  }
}

typedef void (*hsh32map32_functor)(Idx1it * it64, uint64_t val64, uint32_t * val32);

static bool hsh32map32_impl
(
  Hsh1cont * cont,
  Idx1it * it64,
  uint32_t key,
  uint32_t * val32,
  uint32_t bucket_no,
  hsh32map32_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it64 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it64); idx1it_increment(it64))
    {
      uint64_t val64 = idx64it_getval(it64);
      if ((uint32_t)val64 != key)
        continue;
      func(it64, val64, val32);
      return true;
    }
  }
  return false;
}

void hsh32map32_do_none(Idx1it * it64, uint64_t val64, uint32_t * val32)
{}

Hsh1cont hsh32map32_new()
{
  return hsh1cont_new(64, Hsh1valtype_plain);
}

bool hsh32map32_add(Hsh1cont * cont, uint32_t key, uint32_t val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it64;
  if (!hsh32map32_impl(cont, &it64, key, &val, bucket_no, hsh32map32_do_none))
  {
    uint64_t key32val32 = key | ((uint64_t)val << 32);
    idx64cont_append(&cont->bucket[bucket_no], key32val32);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim64, hsh32map32_rehash);
    return true;
  }
  return false;
}

static void hsh32map32_do_update(Idx1it * it64, uint64_t val64, uint32_t * val32)
{
  val64 = (uint32_t)val64;
  val64 |= (uint64_t)*val32 << 32;
  idx64it_setval(it64, val64);
}

void hsh32map32_set(Hsh1cont * cont, uint32_t key, uint32_t val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it64;
  if (!hsh32map32_impl(cont, &it64, key, &val, bucket_no, hsh32map32_do_update))
  {
    uint64_t key32val32 = key | ((uint64_t)val << 32);
    idx64cont_append(&cont->bucket[bucket_no], key32val32);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim64, hsh32map32_rehash);
  }
}

static void hsh32map32_do_ret_copy(Idx1it * it64, uint64_t val64, uint32_t * val32)
{
  *val32 = val64 >> 32;
}

bool hsh32map32_get(Hsh1cont * cont, uint32_t key, uint32_t * out_copy)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it64;
  *out_copy = 0;
  return hsh32map32_impl(cont, &it64, key, out_copy, bucket_no, hsh32map32_do_ret_copy);
}

bool hsh32map32_del(Hsh1cont * cont, uint32_t key)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it64;
  if (hsh32map32_impl(cont, &it64, key, NULL, bucket_no, hsh32map32_do_none))
  {
    idx64cont_remove(&cont->bucket[bucket_no], &it64);
    return true;
  }
  return false;
}

//Hsh64map32 methods family

static void hsh64map32_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it96 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it96))
  {
    Idx1cont val96 = idx96it_getval(&it96);
    uint32_t key32 = val96.end.chunk ^ val96.end.count;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx96cont_remove(&cont->bucket[cont->S], &it96);
      idx96cont_append(&cont->bucket[bucket_no], val96);
      continue;
    }
    idx1it_increment(&it96);
  }
}

typedef void (*hsh64map32_functor)(Idx1it * it96, Idx1cont * val96, uint32_t * val32);

static bool hsh64map32_impl
(
  Hsh1cont * cont,
  Idx1it * it96,
  uint64_t key,
  uint32_t * val32,
  uint32_t bucket_no,
  hsh64map32_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it96 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it96); idx1it_increment(it96))
    {
      Idx1cont val96 = idx96it_getval(it96);
      uint64_t * key64 = (uint64_t *)&val96.end;
      if (*key64 != key)
        continue;
      func(it96, &val96, val32);
      return true;
    }
  }
  return false;
}

void hsh64map32_do_none(Idx1it * it96, Idx1cont * val96, uint32_t * val32)
{}

Hsh1cont hsh64map32_new()
{
  return hsh1cont_new(96, Hsh1valtype_plain);
}

bool hsh64map32_add(Hsh1cont * cont, uint64_t key, uint32_t val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it96;
  if (!hsh64map32_impl(cont, &it96, key, &val, bucket_no, hsh64map32_do_none))
  {
    Idx1it * end = (Idx1it *)&key;
    Idx1cont key64val32 = { .base_idx = val, .end = *end };
    idx96cont_append(&cont->bucket[bucket_no], key64val32);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim96, hsh64map32_rehash);
    return true;
  }
  return false;
}

static void hsh64map32_do_update(Idx1it * it96, Idx1cont * val96, uint32_t * val32)
{
  val96->base_idx  = *val32;
  idx96it_setval(it96, *val96);
}

void hsh64map32_set(Hsh1cont * cont, uint64_t key, uint32_t val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it96;
  if (!hsh64map32_impl(cont, &it96, key, &val, bucket_no, hsh64map32_do_update))
  {
    Idx1it * end = (Idx1it *)&key;
    Idx1cont key64val32 = { .base_idx = val, .end = *end };
    idx96cont_append(&cont->bucket[bucket_no], key64val32);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim96, hsh64map32_rehash);
  }
}

static void hsh64map32_do_ret_copy(Idx1it * it96, Idx1cont * val96, uint32_t * val32)
{
  *val32 = val96->base_idx;
}

bool hsh64map32_get(Hsh1cont * cont, uint64_t key, uint32_t * out_copy)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it96;
  *out_copy = 0;
  return hsh64map32_impl(cont, &it96, key, out_copy, bucket_no, hsh64map32_do_ret_copy);
}

bool hsh64map32_del(Hsh1cont * cont, uint64_t key)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it96;
  if (hsh64map32_impl(cont, &it96, key, NULL, bucket_no, hsh64map32_do_none))
  {
    idx96cont_remove(&cont->bucket[bucket_no], &it96);
    return true;
  }
  return false;
}

//Hsh32map64 methods family

static void hsh32map64_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it96 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it96))
  {
    Idx1cont val96 = idx96it_getval(&it96);
    uint32_t key32 = val96.base_idx;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx96cont_remove(&cont->bucket[cont->S], &it96);
      idx96cont_append(&cont->bucket[bucket_no], val96);
      continue;
    }
    idx1it_increment(&it96);
  }
}

typedef void (*hsh32map64_functor)(Idx1it * it96, Idx1cont * val96, uint64_t * val64);

static bool hsh32map64_impl
(
  Hsh1cont * cont,
  Idx1it * it96,
  uint32_t key,
  uint64_t * val64,
  uint32_t bucket_no,
  hsh32map64_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it96 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it96); idx1it_increment(it96))
    {
      Idx1cont val96 = idx96it_getval(it96);
      uint32_t key32 = val96.base_idx;
      if (key32 != key)
        continue;
      func(it96, &val96, val64);
      return true;
    }
  }
  return false;
}

void hsh32map64_do_none(Idx1it * it96, Idx1cont * val96, uint64_t * val64)
{}

Hsh1cont hsh32map64_new()
{
  return hsh1cont_new(96, Hsh1valtype_plain);
}

bool hsh32map64_add(Hsh1cont * cont, uint32_t key, uint64_t val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it96;
  if (!hsh32map64_impl(cont, &it96, key, &val, bucket_no, hsh32map64_do_none))
  {
    Idx1it * end = (Idx1it *)&val;
    Idx1cont key32val64 = { .base_idx = key, .end = *end };
    idx96cont_append(&cont->bucket[bucket_no], key32val64);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim96, hsh32map64_rehash);
    return true;
  }
  return false;
}

static void hsh32map64_do_update(Idx1it * it96, Idx1cont * val96, uint64_t * val64)
{
  Idx1it * end = (Idx1it *)val64;
  val96->end = *end;
  idx96it_setval(it96, *val96);
}

void hsh32map64_set(Hsh1cont * cont, uint32_t key, uint64_t val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it96;
  if (!hsh32map64_impl(cont, &it96, key, &val, bucket_no, hsh32map64_do_update))
  {
    Idx1it * end = (Idx1it *)&val;
    Idx1cont key32val64 = { .base_idx = key, .end = *end };
    idx96cont_append(&cont->bucket[bucket_no], key32val64);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim96, hsh32map64_rehash);
  }
}

static void hsh32map64_do_ret_copy(Idx1it * it96, Idx1cont * val96, uint64_t * val64)
{
  *val64 = *(uint64_t *)&val96->end;
}

bool hsh32map64_get(Hsh1cont * cont, uint32_t key, uint64_t * out_copy)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it96;
  *out_copy = 0ULL;
  return hsh32map64_impl(cont, &it96, key, out_copy, bucket_no, hsh32map64_do_ret_copy);
}

bool hsh32map64_del(Hsh1cont * cont, uint32_t key)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it96;
  if (hsh32map64_impl(cont, &it96, key, NULL, bucket_no, hsh32map64_do_none))
  {
    idx96cont_remove(&cont->bucket[bucket_no], &it96);
    return true;
  }
  return false;
}

//Hsh64map64 methods family

static void hsh64map64_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it128 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it128))
  {
    Idx1range val128 = idx128it_getval(&it128);
    uint32_t key32 = val128.begin.chunk ^ val128.begin.count;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx128cont_remove(&cont->bucket[cont->S], &it128);
      idx128cont_append(&cont->bucket[bucket_no], val128);
      continue;
    }
    idx1it_increment(&it128);
  }
}

typedef void (*hsh64map64_functor)(Idx1it * it128, Idx1range * val128, uint64_t * val64);

static bool hsh64map64_impl
(
  Hsh1cont * cont,
  Idx1it * it128,
  uint64_t key,
  uint64_t * val64,
  uint32_t bucket_no,
  hsh64map64_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it128 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it128); idx1it_increment(it128))
    {
      Idx1range val128 = idx128it_getval(it128);
      uint64_t * key64 = (uint64_t *)&val128.begin;
      if (*key64 != key)
        continue;
      func(it128, &val128, val64);
      return true;
    }
  }
  return false;
}

void hsh64map64_do_none(Idx1it * it128, Idx1range * val128, uint64_t * val64)
{}

Hsh1cont hsh64map64_new()
{
  return hsh1cont_new(128, Hsh1valtype_plain);
}

bool hsh64map64_add(Hsh1cont * cont, uint64_t key, uint64_t val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it128;
  if (!hsh64map64_impl(cont, &it128, key, &val, bucket_no, hsh64map64_do_none))
  {
    Idx1it * begin = (Idx1it *)&key;
    Idx1it * end = (Idx1it *)&val;
    Idx1range key64val64 = { .begin = *begin, .end = *end };
    idx128cont_append(&cont->bucket[bucket_no], key64val64);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim128, hsh64map64_rehash);
    return true;
  }
  return false;
}

static void hsh64map64_do_update(Idx1it * it128, Idx1range * val128, uint64_t * val64)
{
  Idx1it * end = (Idx1it *)val64;
  val128->end = *end;
  idx128it_setval(it128, *val128);
}

void hsh64map64_set(Hsh1cont * cont, uint64_t key, uint64_t val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it128;
  if (!hsh64map64_impl(cont, &it128, key, &val, bucket_no, hsh64map64_do_update))
  {
    Idx1it * begin = (Idx1it *)&key;
    Idx1it * end = (Idx1it *)&val;
    Idx1range key64val64 = { .begin = *begin, .end = *end };
    idx128cont_append(&cont->bucket[bucket_no], key64val64);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim128, hsh64map64_rehash);
  }
}

static void hsh64map64_do_ret_copy(Idx1it * it128, Idx1range * val128, uint64_t * val64)
{
  *val64 = *(uint64_t *)&val128->end;
}

bool hsh64map64_get(Hsh1cont * cont, uint64_t key, uint64_t * out_copy)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it128;
  *out_copy = 0ULL;
  return hsh64map64_impl(cont, &it128, key, out_copy, bucket_no, hsh64map64_do_ret_copy);
}

bool hsh64map64_del(Hsh1cont * cont, uint64_t key)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it128;
  if (hsh64map64_impl(cont, &it128, key, NULL, bucket_no, hsh64map64_do_none))
  {
    idx128cont_remove(&cont->bucket[bucket_no], &it128);
    return true;
  }
  return false;
}

//Hsh32map96 methods family

static void hsh32map96_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it128 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it128))
  {
    Idx1range val128 = idx128it_getval(&it128);
    uint32_t key32 = val128.begin.chunk;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx128cont_remove(&cont->bucket[cont->S], &it128);
      idx128cont_append(&cont->bucket[bucket_no], val128);
      continue;
    }
    idx1it_increment(&it128);
  }
}

typedef void (*hsh32map96_functor)(Idx1it * it128, Idx1range * val128, Idx1cont * val96);

static bool hsh32map96_impl
(
  Hsh1cont * cont,
  Idx1it * it128,
  uint32_t key,
  Idx1cont * val96,
  uint32_t bucket_no,
  hsh32map96_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it128 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it128); idx1it_increment(it128))
    {
      Idx1range val128 = idx128it_getval(it128);
      uint32_t key32 = val128.begin.chunk;
      if (key32 != key)
        continue;
      func(it128, &val128, val96);
      return true;
    }
  }
  return false;
}

void hsh32map96_do_none(Idx1it * it128, Idx1range * val128, Idx1cont * val96)
{}

Hsh1cont hsh32map96_new(bool clean_val_cont)
{
  return hsh1cont_new(128, clean_val_cont ? Hsh1valtype_cont : Hsh1valtype_plain);
}

bool hsh32map96_add(Hsh1cont * cont, uint32_t key, Idx1cont val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it128;
  if (!hsh32map96_impl(cont, &it128, key, &val, bucket_no, hsh32map96_do_none))
  {
    Idx1range key32val96 = { .begin = { .chunk = key, .count = val.base_idx }, .end = val.end };
    idx128cont_append(&cont->bucket[bucket_no], key32val96);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim128, hsh32map96_rehash);
    return true;
  }
  return false;
}

static void hsh32map96_do_update(Idx1it * it128, Idx1range * val128, Idx1cont * val96)
{
  val128->begin.count = val96->base_idx;
  val128->end = val96->end;
  idx128it_setval(it128, *val128);
}

void hsh32map96_set(Hsh1cont * cont, uint32_t key, Idx1cont val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it128;
  if (!hsh32map96_impl(cont, &it128, key, &val, bucket_no, hsh32map96_do_update))
  {
    Idx1range key32val96 = { .begin = { .chunk = key, .count = val.base_idx }, .end = val.end };
    idx128cont_append(&cont->bucket[bucket_no], key32val96);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim128, hsh32map96_rehash);
  }
}

static void hsh32map96_do_ret_copy(Idx1it * it128, Idx1range * val128, Idx1cont * val96)
{
  *val96 = (Idx1cont){ .base_idx = val128->begin.count, .end = val128->end };
}

bool hsh32map96_get(Hsh1cont * cont, uint32_t key, Idx1cont * out_copy)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it128;
  *out_copy = idx1cont_zero();
  return hsh32map96_impl(cont, &it128, key, out_copy, bucket_no, hsh32map96_do_ret_copy);
}

bool hsh32map96_del(Hsh1cont * cont, uint32_t key)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it128;
  if (hsh32map96_impl(cont, &it128, key, NULL, bucket_no, hsh32map96_do_none))
  {
    if (cont->C)
    {
      Idx1range range = idx128it_getval(&it128);
      Idx1cont cont = { .base_idx = range.begin.count, .end = range.end };
      idx1cont_release(&cont);
    }
    idx128cont_remove(&cont->bucket[bucket_no], &it128);
    return true;
  }
  return false;
}

//Hsh64map96 methods family

static void hsh64map96_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it160 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it160))
  {
    Idx1nonsense1 val160 = idx160it_getval(&it160);
    uint32_t key32 = val160.end.chunk ^ val160.end.count;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx160cont_remove(&cont->bucket[cont->S], &it160);
      idx160cont_append(&cont->bucket[bucket_no], val160);
      continue;
    }
    idx1it_increment(&it160);
  }
}

typedef void (*hsh64map96_functor)(Idx1it * it160, Idx1nonsense1 * val160, Idx1cont * val96);

static bool hsh64map96_impl
(
  Hsh1cont * cont,
  Idx1it * it160,
  uint64_t key,
  Idx1cont * val96,
  uint32_t bucket_no,
  hsh64map96_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it160 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it160); idx1it_increment(it160))
    {
      Idx1nonsense1 val160 = idx160it_getval(it160);
      uint64_t * key64 = (uint64_t *)&val160.end;
      if (*key64 != key)
        continue;
      func(it160, &val160, val96);
      return true;
    }
  }
  return false;
}

void hsh64map96_do_none(Idx1it * it160, Idx1nonsense1 * val160, Idx1cont * val96)
{}

Hsh1cont hsh64map96_new(bool clean_val_cont)
{
  return hsh1cont_new(160, clean_val_cont ? Hsh1valtype_cont : Hsh1valtype_plain);
}

bool hsh64map96_add(Hsh1cont * cont, uint64_t key, Idx1cont val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it160;
  if (!hsh64map96_impl(cont, &it160, key, &val, bucket_no, hsh64map96_do_none))
  {
    Idx1it * end = (Idx1it *)&key;
    Idx1nonsense1 key64val96 = { .cont = val, .end = *end };
    idx160cont_append(&cont->bucket[bucket_no], key64val96);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim160, hsh64map96_rehash);
    return true;
  }
  return false;
}

static void hsh64map96_do_update(Idx1it * it160, Idx1nonsense1 * val160, Idx1cont * val96)
{
  val160->cont = *val96;
  idx160it_setval(it160, *val160);
}

void hsh64map96_set(Hsh1cont * cont, uint64_t key, Idx1cont val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it160;
  if (!hsh64map96_impl(cont, &it160, key, &val, bucket_no, hsh64map96_do_update))
  {
    Idx1it * end = (Idx1it *)&key;
    Idx1nonsense1 key64val96 = { .cont = val, .end = *end };
    idx160cont_append(&cont->bucket[bucket_no], key64val96);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim160, hsh64map96_rehash);
  }
}

static void hsh64map96_do_ret_copy(Idx1it * it160, Idx1nonsense1 * val160, Idx1cont * val96)
{
  *val96 = val160->cont;
}

bool hsh64map96_get(Hsh1cont * cont, uint64_t key, Idx1cont * out_copy)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it160;
  *out_copy = idx1cont_zero();
  return hsh64map96_impl(cont, &it160, key, out_copy, bucket_no, hsh64map96_do_ret_copy);
}

bool hsh64map96_del(Hsh1cont * cont, uint64_t key)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it160;
  if (hsh64map96_impl(cont, &it160, key, NULL, bucket_no, hsh64map96_do_none))
  {
    if (cont->C)
    {
      Idx1nonsense1 nons = idx160it_getval(&it160);
      idx1cont_release(&nons.cont);
    }
    idx160cont_remove(&cont->bucket[bucket_no], &it160);
    return true;
  }
  return false;
}

//Hsh32map128 methods family

static void hsh32map128_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it160 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it160))
  {
    Idx1nonsense1 val160 = idx160it_getval(&it160);
    uint32_t key32 = val160.cont.base_idx;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx160cont_remove(&cont->bucket[cont->S], &it160);
      idx160cont_append(&cont->bucket[bucket_no], val160);
      continue;
    }
    idx1it_increment(&it160);
  }
}

typedef void (*hsh32map128_functor)(Idx1it * it160, Idx1nonsense1 * val160, Idx1range * val128);

static bool hsh32map128_impl
(
  Hsh1cont * cont,
  Idx1it * it160,
  uint32_t key,
  Idx1range * val128,
  uint32_t bucket_no,
  hsh32map128_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it160 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it160); idx1it_increment(it160))
    {
      Idx1nonsense1 val160 = idx160it_getval(it160);
      uint32_t key32 = val160.cont.base_idx;
      if (key32 != key)
        continue;
      func(it160, &val160, val128);
      return true;
    }
  }
  return false;
}

void hsh32map128_do_none(Idx1it * it160, Idx1nonsense1 * val160, Idx1range * val128)
{}

Hsh1cont hsh32map128_new(bool clean_val_queue)
{
  return hsh1cont_new(160, clean_val_queue ? Hsh1valtype_queue : Hsh1valtype_plain);
}

bool hsh32map128_add(Hsh1cont * cont, uint32_t key, Idx1range val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it160;
  if (!hsh32map128_impl(cont, &it160, key, &val, bucket_no, hsh32map128_do_none))
  {
    Idx1nonsense1 key32val128 = { .cont = { .base_idx = key, .end = val.begin }, .end = val.end };
    idx160cont_append(&cont->bucket[bucket_no], key32val128);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim160, hsh32map128_rehash);
    return true;
  }
  return false;
}

static void hsh32map128_do_update(Idx1it * it160, Idx1nonsense1 * val160, Idx1range * val128)
{
  val160->cont.end = val128->begin;
  val160->end = val128->end;
  idx160it_setval(it160, *val160);
}

void hsh32map128_set(Hsh1cont * cont, uint32_t key, Idx1range val)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it160;
  if (!hsh32map128_impl(cont, &it160, key, &val, bucket_no, hsh32map128_do_update))
  {
    Idx1nonsense1 key32val128 = { .cont = { .base_idx = key, .end = val.begin }, .end = val.end };
    idx160cont_append(&cont->bucket[bucket_no], key32val128);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim160, hsh32map128_rehash);
  }
}

static void hsh32map128_do_ret_copy(Idx1it * it160, Idx1nonsense1 * val160, Idx1range * val128)
{
  *val128 = (Idx1range){ .begin = val160->cont.end, .end = val160->end };
}

bool hsh32map128_get(Hsh1cont * cont, uint32_t key, Idx1range * out_copy)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it160;
  *out_copy = idx1range_zero();
  return hsh32map128_impl(cont, &it160, key, out_copy, bucket_no, hsh32map128_do_ret_copy);
}

bool hsh32map128_del(Hsh1cont * cont, uint32_t key)
{
  uint32_t bucket_no = hsh32key_to_bucket_no(cont, key);
  Idx1it it160;
  if (hsh32map128_impl(cont, &it160, key, NULL, bucket_no, hsh32map128_do_none))
  {
    if (cont->Q)
    {
      Idx1nonsense1 nons = idx160it_getval(&it160);
      Idx1range range = { .begin = nons.cont.end, .end = nons.end };
      idx1queue_release(&range);
    }
    idx160cont_remove(&cont->bucket[bucket_no], &it160);
    return true;
  }
  return false;
}

//Hsh64map128 methods family

static void hsh64map128_rehash(Hsh1cont * cont)
{
  uint32_t mask = hsh1cont_N * (1 << cont->L) * 2 - 1;
  Idx1it it192 = idx1it_begin(&cont->bucket[cont->S]);
  while (idx1it_valid(&it192))
  {
    Idx1nonsense2 val192 = idx192it_getval(&it192);
    uint32_t key32 = val192.end.chunk ^ val192.end.count;
    uint32_t bucket_no = key32 & mask;
    if (bucket_no != cont->S)
    {
      idx192cont_remove(&cont->bucket[cont->S], &it192);
      idx192cont_append(&cont->bucket[bucket_no], val192);
      continue;
    }
    idx1it_increment(&it192);
  }
}

typedef void (*hsh64map128_functor)(Idx1it * it192, Idx1nonsense2 * val192, Idx1range * val128);

static bool hsh64map128_impl
(
  Hsh1cont * cont,
  Idx1it * it192,
  uint64_t key,
  Idx1range * val128,
  uint32_t bucket_no,
  hsh64map128_functor func
)
{
  if (cont->bucket[bucket_no].base_idx & 0xffffff)
  {
    for (*it192 = idx1it_begin(&cont->bucket[bucket_no]); idx1it_valid(it192); idx1it_increment(it192))
    {
      Idx1nonsense2 val192 = idx192it_getval(it192);
      uint64_t * key64 = (uint64_t *)&val192.end;
      if (*key64 != key)
        continue;
      func(it192, &val192, val128);
      return true;
    }
  }
  return false;
}

void hsh64map128_do_none(Idx1it * it192, Idx1nonsense2 * val192, Idx1range * val128)
{}

Hsh1cont hsh64map128_new(bool clean_val_queue)
{
  return hsh1cont_new(192, clean_val_queue ? Hsh1valtype_queue : Hsh1valtype_plain);
}

bool hsh64map128_add(Hsh1cont * cont, uint64_t key, Idx1range val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it192;
  if (!hsh64map128_impl(cont, &it192, key, &val, bucket_no, hsh64map128_do_none))
  {
    Idx1it * end = (Idx1it *)&key;
    Idx1nonsense2 key64val128 = { .range = val, .end = *end };
    idx192cont_append(&cont->bucket[bucket_no], key64val128);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim192, hsh64map128_rehash);
    return true;
  }
  return false;
}

static void hsh64map128_do_update(Idx1it * it192, Idx1nonsense2 * val192, Idx1range * val128)
{
  val192->range = *val128;
  idx192it_setval(it192, *val192);
}

void hsh64map128_set(Hsh1cont * cont, uint64_t key, Idx1range val)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it192;
  if (!hsh64map128_impl(cont, &it192, key, &val, bucket_no, hsh64map128_do_update))
  {
    Idx1it * end = (Idx1it *)&key;
    Idx1nonsense2 key64val128 = { .range = val, .end = *end };
    idx192cont_append(&cont->bucket[bucket_no], key64val128);
    hsh1cont_add_check(cont, bucket_no, Hsh1bucket_lim192, hsh64map128_rehash);
  }
}

static void hsh64map128_do_ret_copy(Idx1it * it192, Idx1nonsense2 * val192, Idx1range * val128)
{
  *val128 = val192->range;
}

bool hsh64map128_get(Hsh1cont * cont, uint64_t key, Idx1range * out_copy)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it192;
  *out_copy = idx1range_zero();
  return hsh64map128_impl(cont, &it192, key, out_copy, bucket_no, hsh64map128_do_ret_copy);
}

bool hsh64map128_del(Hsh1cont * cont, uint64_t key)
{
  uint32_t bucket_no = hsh64key_to_bucket_no(cont, key);
  Idx1it it192;
  if (hsh64map128_impl(cont, &it192, key, NULL, bucket_no, hsh64map128_do_none))
  {
    if (cont->Q)
    {
      Idx1nonsense2 nons = idx192it_getval(&it192);
      idx1queue_release(&nons.range);
    }
    idx192cont_remove(&cont->bucket[bucket_no], &it192);
    return true;
  }
  return false;
}
