
#pragma once

#include "idx1pool.h"

typedef struct Hsh1cont_t
{
  Idx1cont * bucket;
  uint32_t num_buckets;
  //linear hash parameters
  uint32_t S;
  uint8_t  L;
  //non-pod flags container/queue
  bool     C;
  bool     Q;
}
Hsh1cont;

//delete "generic" hash table
void hsh1cont_release(Hsh1cont * cont);

//remove all data
void hsh1cont_clear(Hsh1cont * cont);

//"generic" hash table deep copy
Hsh1cont hsh1cont_copy(Hsh1cont * from);

//Hsh32set methods family
Hsh1cont hsh32set_new();
bool hsh32set_add(Hsh1cont * cont, uint32_t key);
bool hsh32set_has(Hsh1cont * cont, uint32_t key);
bool hsh32set_del(Hsh1cont * cont, uint32_t key);

//Hsh64set methods family
Hsh1cont hsh64set_new();
bool hsh64set_add(Hsh1cont * cont, uint64_t key);
bool hsh64set_has(Hsh1cont * cont, uint64_t key);
bool hsh64set_del(Hsh1cont * cont, uint64_t key);

//Hsh32map32 methods family
Hsh1cont hsh32map32_new();
bool hsh32map32_add(Hsh1cont * cont, uint32_t key, uint32_t val);
void hsh32map32_set(Hsh1cont * cont, uint32_t key, uint32_t val);
bool hsh32map32_get(Hsh1cont * cont, uint32_t key, uint32_t * out_copy);
bool hsh32map32_del(Hsh1cont * cont, uint32_t key);

//Hsh64map32 methods family
Hsh1cont hsh64map32_new();
bool hsh64map32_add(Hsh1cont * cont, uint64_t key, uint32_t val);
void hsh64map32_set(Hsh1cont * cont, uint64_t key, uint32_t val);
bool hsh64map32_get(Hsh1cont * cont, uint64_t key, uint32_t * out_copy);
bool hsh64map32_del(Hsh1cont * cont, uint64_t key);

//Hsh32map64 methods family
Hsh1cont hsh32map64_new();
bool hsh32map64_add(Hsh1cont * cont, uint32_t key, uint64_t val);
void hsh32map64_set(Hsh1cont * cont, uint32_t key, uint64_t val);
bool hsh32map64_get(Hsh1cont * cont, uint32_t key, uint64_t * out_copy);
bool hsh32map64_del(Hsh1cont * cont, uint32_t key);

//Hsh64map64 methods family
Hsh1cont hsh64map64_new();
bool hsh64map64_add(Hsh1cont * cont, uint64_t key, uint64_t val);
void hsh64map64_set(Hsh1cont * cont, uint64_t key, uint64_t val);
bool hsh64map64_get(Hsh1cont * cont, uint64_t key, uint64_t * out_copy);
bool hsh64map64_del(Hsh1cont * cont, uint64_t key);

//Hsh32map96 methods family
Hsh1cont hsh32map96_new(bool clean_val_cont);
bool hsh32map96_add(Hsh1cont * cont, uint32_t key, Idx1cont val);
void hsh32map96_set(Hsh1cont * cont, uint32_t key, Idx1cont val);
bool hsh32map96_get(Hsh1cont * cont, uint32_t key, Idx1cont * out_copy);
bool hsh32map96_del(Hsh1cont * cont, uint32_t key);

//Hsh64map96 methods family
Hsh1cont hsh64map96_new(bool clean_val_cont);
bool hsh64map96_add(Hsh1cont * cont, uint64_t key, Idx1cont val);
void hsh64map96_set(Hsh1cont * cont, uint64_t key, Idx1cont val);
bool hsh64map96_get(Hsh1cont * cont, uint64_t key, Idx1cont * out_copy);
bool hsh64map96_del(Hsh1cont * cont, uint64_t key);

//Hsh32map128 methods family
Hsh1cont hsh32map128_new(bool clean_val_queue);
bool hsh32map128_add(Hsh1cont * cont, uint32_t key, Idx1range val);
void hsh32map128_set(Hsh1cont * cont, uint32_t key, Idx1range val);
bool hsh32map128_get(Hsh1cont * cont, uint32_t key, Idx1range * out_copy);
bool hsh32map128_del(Hsh1cont * cont, uint32_t key);

//Hsh64map128 methods family
Hsh1cont hsh64map128_new(bool clean_val_queue);
bool hsh64map128_add(Hsh1cont * cont, uint64_t key, Idx1range val);
void hsh64map128_set(Hsh1cont * cont, uint64_t key, Idx1range val);
bool hsh64map128_get(Hsh1cont * cont, uint64_t key, Idx1range * out_copy);
bool hsh64map128_del(Hsh1cont * cont, uint64_t key);
