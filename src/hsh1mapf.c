
#include "hsh1mapf.h"


//Hsh32map32f methods family

Hsh1cont hsh32map32f_new()
{
  return hsh32map32_new();
}

bool hsh32map32f_add(Hsh1cont * cont, uint32_t key, float val)
{
  return hsh32map32_add(cont, key, *((uint32_t*)&val));
}

void hsh32map32f_set(Hsh1cont * cont, uint32_t key, float val)
{
  return hsh32map32_set(cont, key, *((uint32_t*)&val));
}

bool hsh32map32f_get(Hsh1cont * cont, uint32_t key, float * out_copy)
{
  return hsh32map32_get(cont, key, (uint32_t*)out_copy);
}

bool hsh32map32f_del(Hsh1cont * cont, uint32_t key)
{
  hsh32map32_del(cont, key);
}

//Hsh32map32f2 methods family

Hsh1cont hsh32map32f2_new()
{
  return hsh32map64_new();
}

bool hsh32map32f2_add(Hsh1cont * cont, uint32_t key, float val[2])
{
  return hsh32map64_add(cont, key, *((uint64_t*)val));
}

void hsh32map32f2_set(Hsh1cont * cont, uint32_t key, float val[2])
{
  return hsh32map64_set(cont, key, *((uint64_t*)val));
}

bool hsh32map32f2_get(Hsh1cont * cont, uint32_t key, float * out_copy[2])
{
  return hsh32map64_get(cont, key, (uint64_t*)out_copy);
}

bool hsh32map32f2_del(Hsh1cont * cont, uint32_t key)
{
  return hsh32map64_del(cont, key);
}

//Hsh32map32f3 methods family

Hsh1cont hsh32map32f3_new()
{
  return hsh32map96_new(false/*clean_val_cont*/);
}

bool hsh32map32f3_add(Hsh1cont * cont, uint32_t key, float val[3])
{
  return hsh32map96_add(cont, key, *((Idx1cont*)val));
}

void hsh32map32f3_set(Hsh1cont * cont, uint32_t key, float val[3])
{
  return hsh32map96_set(cont, key, *((Idx1cont*)val));
}

bool hsh32map32f3_get(Hsh1cont * cont, uint32_t key, float * out_copy[3])
{
  return hsh32map96_get(cont, key, (Idx1cont*)out_copy);
}

bool hsh32map32f3_del(Hsh1cont * cont, uint32_t key)
{
  return hsh32map96_del(cont, key);
}

//Hsh32map32f4 methods family

Hsh1cont hsh32map32f4_new()
{
  return hsh32map128_new(false/*clean_val_queue*/);
}

bool hsh32map32f4_add(Hsh1cont * cont, uint32_t key, float val[4])
{
  return hsh32map128_add(cont, key, *((Idx1range*)val));
}

void hsh32map32f4_set(Hsh1cont * cont, uint32_t key, float val[4])
{
  return hsh32map128_set(cont, key, *((Idx1range*)val));
}

bool hsh32map32f4_get(Hsh1cont * cont, uint32_t key, float * out_copy[4])
{
  return hsh32map128_get(cont, key, (Idx1range*)out_copy);
}

bool hsh32map32f4_del(Hsh1cont * cont, uint32_t key)
{
  return hsh32map128_del(cont, key);
}

//Hsh64map32f methods family

Hsh1cont hsh64map32f_new()
{
  return hsh64map32_new();
}

bool hsh64map32f_add(Hsh1cont * cont, uint64_t key, float val)
{
  return hsh64map32_add(cont, key, *((uint32_t*)&val));
}

void hsh64map32f_set(Hsh1cont * cont, uint64_t key, float val)
{
  return hsh64map32_set(cont, key, *((uint32_t*)&val));
}

bool hsh64map32f_get(Hsh1cont * cont, uint64_t key, float * out_copy)
{
  return hsh64map32_get(cont, key, (uint32_t*)out_copy);
}

bool hsh64map32f_del(Hsh1cont * cont, uint64_t key)
{
  hsh64map32_del(cont, key);
}

//Hsh64map32f2 methods family

Hsh1cont hsh64map32f2_new()
{
  return hsh64map64_new();
}

bool hsh64map32f2_add(Hsh1cont * cont, uint64_t key, float val[2])
{
  return hsh64map64_add(cont, key, *((uint64_t*)val));
}

void hsh64map32f2_set(Hsh1cont * cont, uint64_t key, float val[2])
{
  return hsh64map64_set(cont, key, *((uint64_t*)val));
}

bool hsh64map32f2_get(Hsh1cont * cont, uint64_t key, float * out_copy[2])
{
  return hsh64map64_get(cont, key, (uint64_t*)out_copy);
}

bool hsh64map32f2_del(Hsh1cont * cont, uint64_t key)
{
  return hsh64map64_del(cont, key);
}

//Hsh64map32f3 methods family

Hsh1cont hsh64map32f3_new()
{
  return hsh64map96_new(false/*clean_val_cont*/);
}

bool hsh64map32f3_add(Hsh1cont * cont, uint64_t key, float val[3])
{
  return hsh64map96_add(cont, key, *((Idx1cont*)val));
}

void hsh64map32f3_set(Hsh1cont * cont, uint64_t key, float val[3])
{
  return hsh64map96_set(cont, key, *((Idx1cont*)val));
}

bool hsh64map32f3_get(Hsh1cont * cont, uint64_t key, float * out_copy[3])
{
  return hsh64map96_get(cont, key, (Idx1cont*)out_copy);
}

bool hsh64map32f3_del(Hsh1cont * cont, uint64_t key)
{
  return hsh64map96_del(cont, key);
}

//Hsh64map32f4 methods family

Hsh1cont hsh64map32f4_new()
{
  return hsh64map128_new(false/*clean_val_queue*/);
}

bool hsh64map32f4_add(Hsh1cont * cont, uint64_t key, float val[4])
{
  return hsh64map128_add(cont, key, *((Idx1range*)val));
}

void hsh64map32f4_set(Hsh1cont * cont, uint64_t key, float val[4])
{
  return hsh64map128_set(cont, key, *((Idx1range*)val));
}

bool hsh64map32f4_get(Hsh1cont * cont, uint64_t key, float * out_copy[4])
{
  return hsh64map128_get(cont, key, (Idx1range*)out_copy);
}

bool hsh64map32f4_del(Hsh1cont * cont, uint64_t key)
{
  return hsh64map128_del(cont, key);
}
