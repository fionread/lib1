
#pragma once

#include "hsh1cont.h"

//Hsh32map32f methods family
Hsh1cont hsh32map32f_new();
bool hsh32map32f_add(Hsh1cont * cont, uint32_t key, float val);
void hsh32map32f_set(Hsh1cont * cont, uint32_t key, float val);
bool hsh32map32f_get(Hsh1cont * cont, uint32_t key, float * out_copy);
bool hsh32map32f_del(Hsh1cont * cont, uint32_t key);

//Hsh32map32f2 methods family
Hsh1cont hsh32map32f2_new();
bool hsh32map32f2_add(Hsh1cont * cont, uint32_t key, float val[2]);
void hsh32map32f2_set(Hsh1cont * cont, uint32_t key, float val[2]);
bool hsh32map32f2_get(Hsh1cont * cont, uint32_t key, float * out_copy[2]);
bool hsh32map32f2_del(Hsh1cont * cont, uint32_t key);

//Hsh32map32f3 methods family
Hsh1cont hsh32map32f3_new();
bool hsh32map32f3_add(Hsh1cont * cont, uint32_t key, float val[3]);
void hsh32map32f3_set(Hsh1cont * cont, uint32_t key, float val[3]);
bool hsh32map32f3_get(Hsh1cont * cont, uint32_t key, float * out_copy[3]);
bool hsh32map32f3_del(Hsh1cont * cont, uint32_t key);

//Hsh32map32f4 methods family
Hsh1cont hsh32map32f4_new();
bool hsh32map32f4_add(Hsh1cont * cont, uint32_t key, float val[4]);
void hsh32map32f4_set(Hsh1cont * cont, uint32_t key, float val[4]);
bool hsh32map32f4_get(Hsh1cont * cont, uint32_t key, float * out_copy[4]);
bool hsh32map32f4_del(Hsh1cont * cont, uint32_t key);

//Hsh64map32f methods family
Hsh1cont hsh64map32f_new();
bool hsh64map32f_add(Hsh1cont * cont, uint64_t key, float val);
void hsh64map32f_set(Hsh1cont * cont, uint64_t key, float val);
bool hsh64map32f_get(Hsh1cont * cont, uint64_t key, float * out_copy);
bool hsh64map32f_del(Hsh1cont * cont, uint64_t key);

//Hsh64map32f2 methods family
Hsh1cont hsh64map32f2_new();
bool hsh64map32f2_add(Hsh1cont * cont, uint64_t key, float val[2]);
void hsh64map32f2_set(Hsh1cont * cont, uint64_t key, float val[2]);
bool hsh64map32f2_get(Hsh1cont * cont, uint64_t key, float * out_copy[2]);
bool hsh64map32f2_del(Hsh1cont * cont, uint64_t key);

//Hsh64map32f3 methods family
Hsh1cont hsh64map32f3_new();
bool hsh64map32f3_add(Hsh1cont * cont, uint64_t key, float val[3]);
void hsh64map32f3_set(Hsh1cont * cont, uint64_t key, float val[3]);
bool hsh64map32f3_get(Hsh1cont * cont, uint64_t key, float * out_copy[3]);
bool hsh64map32f3_del(Hsh1cont * cont, uint64_t key);

//Hsh64map32f4 methods family
Hsh1cont hsh64map32f4_new();
bool hsh64map32f4_add(Hsh1cont * cont, uint64_t key, float val[4]);
void hsh64map32f4_set(Hsh1cont * cont, uint64_t key, float val[4]);
bool hsh64map32f4_get(Hsh1cont * cont, uint64_t key, float * out_copy[4]);
bool hsh64map32f4_del(Hsh1cont * cont, uint64_t key);
