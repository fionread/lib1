
#pragma once

#include "idx1pool.h"

//----------------blanks for Hsh1cont implementation------------------

//do not use them with stack/queue methods nor deep copy methods

typedef struct Idx1nonsense1_t
{
  Idx1cont cont;
  Idx1it end;
}
Idx1nonsense1; //formal 160 bit

//Idx160it methods family
Idx1nonsense1 idx160it_getval(const Idx1it * it);
void idx160it_setval(const Idx1it * it, Idx1nonsense1 val);

//Idx160cont methods family
void idx160cont_append(Idx1cont * cont, Idx1nonsense1 val);
void idx160cont_remove(Idx1cont * cont, Idx1it * it);
Idx1it idx160it_cont_idx(const Idx1cont * cont, int32_t idx);

typedef struct Idx1nonsense2_t
{
  Idx1range range;
  Idx1it end;
}
Idx1nonsense2; //formal 192 bit

//Idx192it methods family
Idx1nonsense2 idx192it_getval(const Idx1it * it);
void idx192it_setval(const Idx1it * it, Idx1nonsense2 val);

//Idx192cont methods family
void idx192cont_append(Idx1cont * cont, Idx1nonsense2 val);
void idx192cont_remove(Idx1cont * cont, Idx1it * it);
Idx1it idx192it_cont_idx(const Idx1cont * cont, int32_t idx);

//iterator decrement implementations (do not use on queue, it will go far before begin)
void idx1it_link2_decrement(Idx1it * it);
void idx1it_link1_decrement(Idx1it * it, uint32_t base_idx, uint8_t chunk_elnum);
