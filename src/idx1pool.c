
#include "idx1internal.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define IDX32CHUNK_MAX (1 << 24)

enum Idx32chunk_en
{
  //reference values to guarantee chunks of any type are 104 bytes length and linked properly
  //may not be changed without consequences
  Idx32chunk_eltot = 26,
  Idx32chunk_nxcnt = Idx32chunk_eltot - 1,
  Idx32chunk_prev  = Idx32chunk_eltot - 2,
  Idx32chunk_elnum = Idx32chunk_prev
};

typedef struct Idx32chunk_t
{
  uint32_t data[Idx32chunk_eltot];
}
Idx32chunk;

static Idx32chunk * idx32pool = NULL;

//0 - reserved for invalid iterators
static uint32_t idx32chunk_head_idx = 1;

//0 - does not fit because it is multiplier on resize
//1 - does not fit because head becomes addressing to none
static uint32_t idx32chunk_max = 2;

static int32_t idx32chunks_busy = 0;

inline uint8_t idx32chunk_getcount(uint32_t data)
{
  return data;
}

inline uint32_t idx32chunk_getnext(uint32_t data)
{
  return data >> 8;
}

inline uint32_t idx32chunk_compose(uint32_t next, uint8_t count)
{
  return (next << 8) | count;
}

static void idx32pool_deallocate()
{
  free(idx32pool);
  idx32pool = NULL;
}

static void idx32pool_deallocate_final()
{
  idx32pool_deallocate();  
  assert(idx32chunks_busy == 0);
}

static void idx32chunk_arr_init(uint32_t old_cnt, uint32_t new_cnt)
{
  uint32_t last_cnt = new_cnt - 1;
  uint32_t u;
  assert(new_cnt);
  assert(old_cnt);
  for (u = old_cnt - 1; u < last_cnt; ++u)
    idx32pool[u].data[Idx32chunk_nxcnt] = idx32chunk_compose(u + 1, 0);
  idx32pool[0].data[Idx32chunk_nxcnt] = 0; //reserved for invalid iterators
  idx32pool[last_cnt].data[Idx32chunk_nxcnt] = 0; //end
}

void idx1pool_reserve(uint32_t count)
{
  Idx32chunk * ptr = NULL;
  assert(count > 1); //see explanation for idx32chunk_max
  assert(count <= IDX32CHUNK_MAX);
  ptr = (Idx32chunk *)malloc(sizeof(Idx32chunk) * count);
  assert(ptr);
  if (idx32pool != NULL)
  {
    memcpy(ptr, idx32pool, sizeof(Idx32chunk) * idx32chunk_max);
    idx32pool_deallocate();
  }
  else
    atexit(idx32pool_deallocate_final);
  idx32pool = ptr;
  idx32chunk_arr_init(idx32chunk_max, count);
  idx32chunk_max = count;
}

static void idx32pool_check()
{
  //lock guard
  if (idx32pool != NULL && idx32chunk_getnext(idx32pool[idx32chunk_head_idx].data[Idx32chunk_nxcnt]))
    return;
  idx1pool_reserve(idx32chunk_max * 2);
}

static uint32_t idx32chunk_new()
{
  //lock guard
  uint32_t res = idx32chunk_head_idx;
  idx32chunk_head_idx = idx32chunk_getnext(idx32pool[res].data[Idx32chunk_nxcnt]);
  assert(idx32chunk_head_idx);
  idx32pool[res].data[Idx32chunk_prev] = 0;
  idx32pool[res].data[Idx32chunk_nxcnt] = 0;
  ++idx32chunks_busy;
  return res;
}

static void idx32chunk_release(uint32_t base_idx)
{
  uint32_t next_idx = base_idx;
  uint32_t prev_idx;
  assert(base_idx);
  do
  {
    prev_idx = next_idx;
    next_idx = idx32chunk_getnext(idx32pool[prev_idx].data[Idx32chunk_nxcnt]);
    //lock guard
    --idx32chunks_busy;
  }
  while (next_idx);
  //lock guard
  idx32pool[prev_idx].data[Idx32chunk_nxcnt] = idx32chunk_compose(idx32chunk_head_idx, 0);
  idx32chunk_head_idx = base_idx;
}

static void idx32chunk_cut_tail(uint32_t prev_idx, uint32_t next_idx)
{
  const uint8_t elnum = idx32chunk_getcount(idx32pool[prev_idx].data[Idx32chunk_nxcnt]);
  idx32pool[prev_idx].data[Idx32chunk_nxcnt] = idx32chunk_compose(0, elnum); 
  assert(next_idx);
  idx32chunk_release(next_idx);
}

static void idx32chunk_cut_nose_link2(uint32_t prev_idx, uint32_t next_idx)
{
  assert(prev_idx);
  idx32pool[prev_idx].data[Idx32chunk_nxcnt] = 0;
  if (next_idx)
    idx32pool[next_idx].data[Idx32chunk_prev] = 0;
  idx32chunk_release(prev_idx);
}

//"Generic" methods family

void idx1cont_release(Idx1cont * cont)
{
  uint32_t base_idx = cont->base_idx & 0xffffff;
  if (base_idx)
  {
    idx32chunk_release(base_idx);
    *cont = (Idx1cont){ .base_idx = cont->base_idx & 0xff000000, .end = idx1it_zero() };
  }
}

void idx1queue_release(Idx1range * range)
{
  Idx1cont cont = { .base_idx = idx32chunk_getnext(range->begin.chunk), .end = range->end };
  idx1cont_release(&cont);
  *range = idx1range_zero();
}

Idx1cont idx1cont_zero()
{
  return (Idx1cont){ .base_idx = 0, .end = idx1it_zero() };
}

Idx1range idx1range_zero()
{
  return (Idx1range){ .begin = idx1it_zero(), .end = idx1it_zero() };
}

Idx1it idx1it_zero()
{
  return (Idx1it){ .chunk = 0, .count = 0 };
}

Idx1cont idx1cont_copy(Idx1cont * from)
{
  uint32_t prev_idx_read;
  uint32_t next_idx_read = from->base_idx & 0xffffff;
  uint32_t start_idx_write;
  uint32_t prev_idx_write = 0;
  uint8_t elnum;
  do
  {
    uint32_t next_idx_write;
    idx32pool_check();
    next_idx_write = idx32chunk_new();
    idx32pool[next_idx_write] = idx32pool[next_idx_read];
    if (prev_idx_write)
    {
      elnum = idx32chunk_getcount(idx32pool[prev_idx_read].data[Idx32chunk_nxcnt]);
      idx32pool[prev_idx_write].data[Idx32chunk_nxcnt] = idx32chunk_compose(next_idx_write, elnum);
      idx32pool[next_idx_write].data[Idx32chunk_prev] = prev_idx_write;
    }
    else
      start_idx_write = next_idx_write;
    prev_idx_write = next_idx_write;
    prev_idx_read = next_idx_read;
    next_idx_read = idx32chunk_getnext(idx32pool[prev_idx_read].data[Idx32chunk_nxcnt]);
  }
  while (next_idx_read);
  elnum = idx32chunk_getcount(idx32pool[prev_idx_write].data[Idx32chunk_nxcnt]);
  return (Idx1cont){
    .base_idx = start_idx_write | (idx1cont_get_bits(from) << 24),
    .end = { .chunk = idx32chunk_compose(prev_idx_write, elnum), .count = from->end.count }
  };
}

Idx1range idx1range_copy(Idx1range * from)
{
  Idx1cont orig = { .base_idx = idx32chunk_getnext(from->begin.chunk), .end = from->end };
  Idx1cont copy = idx1cont_copy(&orig);
  return (Idx1range){
    .begin = {
      .chunk = idx32chunk_compose(copy.base_idx, idx32chunk_getcount(from->begin.chunk)),
      .count = 0
    },
    .end = copy.end
  };
}

static Idx1cont idx1cont_new(uint32_t bits)
{
  uint32_t next_idx;
  idx32pool_check();
  next_idx = idx32chunk_new();
  return (Idx1cont){
    .base_idx = next_idx | (bits << 24),
    .end = { .chunk = idx32chunk_compose(next_idx, 0), .count = 0 }
  };
}

static void idx1cont_lazy_init(Idx1cont * cont, uint32_t bits)
{
  if (cont->base_idx & 0xffffff)
    ;
  else
    *cont = idx1cont_new(bits);
}

static void idx1cont_lazy_deinit(Idx1cont * cont)
{
  if (!idx1stack_empty(cont))
    ;
  else
    idx1cont_release(cont);
}

static void idx1cont_lazy_deinit_iter(Idx1cont * cont, Idx1it * it)
{
  if (!idx1stack_empty(cont))
    ;
  else
  {
    idx1cont_release(cont);
    *it = idx1it_zero();
  }
}

static void idx1range_lazy_init(Idx1range * range)
{
  if (idx32chunk_getnext(range->begin.chunk))
    ;
  else
  {
    Idx1cont cont = idx1cont_new(0);
    *range = (Idx1range){ .begin = cont.end, .end = cont.end };
  }
}

static void idx1range_lazy_init_full(Idx1range * range, uint8_t elnum)
{
  if (idx32chunk_getnext(range->begin.chunk))
    ;
  else
  {
    Idx1cont cont = idx1cont_new(0);
    Idx1it it = { .chunk = idx32chunk_compose(cont.base_idx, elnum - 1), .count = 0 };
    idx32pool[cont.base_idx].data[Idx32chunk_nxcnt] = idx32chunk_compose(0, elnum - 1);
    *range = (Idx1range){ .begin = it, .end = it };
  }
}

static void idx1range_lazy_deinit(Idx1range * range)
{
  if (!idx1queue_empty(range))
    ;
  else
    idx1queue_release(range);
}

static void idx1cont_append_impl(Idx1it * end, uint8_t chunk_elnum, uint8_t chunk_prev)
{
  const uint32_t prev_idx = idx32chunk_getnext(end->chunk);
  uint32_t next_idx = idx32chunk_getnext(idx32pool[prev_idx].data[Idx32chunk_nxcnt]);
  uint32_t chunk_idx = prev_idx;
  uint8_t elnum = idx32chunk_getcount(idx32pool[prev_idx].data[Idx32chunk_nxcnt]);
  uint8_t chunk_el = 0;
  if (++elnum < chunk_elnum)
    chunk_el = elnum;
  else
  {
    if (next_idx == 0)
    {
      idx32pool_check();
      next_idx = idx32chunk_new();
    }
    if (chunk_prev)
      idx32pool[next_idx].data[chunk_prev] = prev_idx;
    chunk_idx = next_idx;
  }
  idx32pool[prev_idx].data[Idx32chunk_nxcnt] = idx32chunk_compose(next_idx, elnum);
  end->chunk = idx32chunk_compose(chunk_idx, chunk_el);
  ++end->count;
}

static void idx1cont_remove_impl(const Idx1it * end, uint32_t last_idx)
{
  const uint32_t prev_idx = idx32chunk_getnext(end->chunk);
  const uint32_t next_idx = idx32chunk_getnext(idx32pool[prev_idx].data[Idx32chunk_nxcnt]);
  const uint8_t elnum = idx32chunk_getcount(idx32pool[prev_idx].data[Idx32chunk_nxcnt]);
  assert(last_idx);
  assert(prev_idx);
  assert(elnum);
  idx32pool[prev_idx].data[Idx32chunk_nxcnt] = idx32chunk_compose(next_idx, elnum - 1);
  if (last_idx != prev_idx)
    idx32chunk_cut_tail(prev_idx, next_idx);
}

static void idx1queue_remove_impl(Idx1it * begin, Idx1it * end, uint32_t last_idx)
{
  const uint32_t next_idx = idx32chunk_getnext(begin->chunk);
  if (last_idx != next_idx)
    idx32chunk_cut_nose_link2(last_idx, next_idx);
  end->count -= begin->count;
  begin->count = 0;
}

static Idx1it idx1cont_find_idx_impl(uint32_t base_idx, int32_t find_idx, uint8_t chunk_elnum)
{
  Idx1it it;
  if (find_idx >= 0)
  {
    const uint8_t frac = find_idx % chunk_elnum;
    uint32_t chunk_idx = base_idx & 0xffffff;
    int32_t seq = find_idx / chunk_elnum;
    while (seq--)
      chunk_idx = idx32chunk_getnext(idx32pool[chunk_idx].data[Idx32chunk_nxcnt]);
    it.chunk = idx32chunk_compose(chunk_idx, frac);
  }
  else
    it.chunk = 0;
  it.count = find_idx;
  return it;
}

static Idx1it idx1range_find_idx_impl(const Idx1it * begin, int32_t find_idx, uint8_t chunk_elnum)
{
  return idx1cont_find_idx_impl(idx32chunk_getnext(begin->chunk),
    find_idx + idx32chunk_getcount(begin->chunk), chunk_elnum);
}

uint8_t idx1cont_get_bits(const Idx1cont * cont)
{
  return cont->base_idx >> 24;
}

void idx1cont_set_bits(Idx1cont * cont, uint8_t bits)
{
  cont->base_idx = (cont->base_idx & 0xffffff) | ((uint32_t)bits << 24);
}

Idx1it idx1it_begin(const Idx1cont * cont)
{
  Idx1it res = { .chunk = idx32chunk_compose(cont->base_idx & 0xffffff, 0), .count = 0 };
  return res;
}

bool idx1it_valid(const Idx1it * it)
{
  return idx32chunk_getcount(it->chunk)
    < idx32chunk_getcount(idx32pool[idx32chunk_getnext(it->chunk)].data[Idx32chunk_nxcnt]);
}

void idx1it_increment(Idx1it * it)
{
  const uint32_t chunk_idx = idx32chunk_getnext(it->chunk);
  uint8_t chunk_el = idx32chunk_getcount(it->chunk);
  const uint8_t elnum = idx32chunk_getcount(idx32pool[chunk_idx].data[Idx32chunk_nxcnt]);
  const uint32_t next_idx = idx32chunk_getnext(idx32pool[chunk_idx].data[Idx32chunk_nxcnt]);
  if (++chunk_el < elnum)
    it->chunk = idx32chunk_compose(chunk_idx, chunk_el);
  else if (next_idx)
    it->chunk = idx32chunk_compose(next_idx, 0);
  else
    it->chunk = 0;
  ++it->count;
}

void idx1it_link2_decrement(Idx1it * it)
{
  const uint32_t chunk_idx = idx32chunk_getnext(it->chunk);
  uint8_t chunk_el = idx32chunk_getcount(it->chunk);
  uint32_t prev_idx = idx32pool[chunk_idx].data[Idx32chunk_prev] & 0xffffff;
  if (chunk_el > 0)
    it->chunk = idx32chunk_compose(chunk_idx, --chunk_el);
  else if (prev_idx)
  {
    chunk_el = idx32chunk_getcount(idx32pool[prev_idx].data[Idx32chunk_nxcnt]);
    assert(chunk_el);
    it->chunk = idx32chunk_compose(prev_idx, --chunk_el);
  }
  else
    it->chunk = 0;
  --it->count;
}

void idx1it_link1_decrement(Idx1it * it, uint32_t base_idx, uint8_t chunk_elnum)
{
  const uint32_t chunk_idx = idx32chunk_getnext(it->chunk);
  uint8_t chunk_el = idx32chunk_getcount(it->chunk);
  if (chunk_el-- > 0)
  {
    it->chunk = idx32chunk_compose(chunk_idx, chunk_el);
    --it->count;
  }
  else
    *it = idx1cont_find_idx_impl(base_idx, it->count - 1, chunk_elnum);
}

static void idx1range_prepend_impl(Idx1it * begin, Idx1it * end, uint8_t chunk_elnum)
{
  const uint32_t next_idx = idx32chunk_getnext(begin->chunk);
  idx1it_link2_decrement(begin);
  if (!idx1it_valid(begin))
  {
    uint32_t prev_idx;
    idx32pool_check();
    prev_idx = idx32chunk_new();
    idx32pool[prev_idx].data[Idx32chunk_nxcnt] = idx32chunk_compose(next_idx, chunk_elnum);
    idx32pool[next_idx].data[Idx32chunk_prev] = prev_idx;
    begin->chunk = idx32chunk_compose(prev_idx, chunk_elnum - 1);
  }
  begin->count = 0;
  ++end->count;
}

//Idx32it methods family

uint32_t idx32it_getval(const Idx1it * it)
{
  return idx32pool[idx32chunk_getnext(it->chunk)].data[idx32chunk_getcount(it->chunk)];
}

void idx32it_setval(const Idx1it * it, uint32_t val)
{
  idx32pool[idx32chunk_getnext(it->chunk)].data[idx32chunk_getcount(it->chunk)] = val;
}

//Idx32cont methods family

void idx32cont_append(Idx1cont * cont, uint32_t val)
{
  idx1cont_lazy_init(cont, 32);
  idx32it_setval(&cont->end, val);
  idx1cont_append_impl(&cont->end, Idx32chunk_elnum, Idx32chunk_prev);
}

void idx32cont_remove(Idx1cont * cont, Idx1it * it)
{
  const uint32_t last_idx = idx32chunk_getnext(cont->end.chunk);
  idx1it_link2_decrement(&cont->end);
  idx32it_setval(it, idx32it_getval(&cont->end));
  idx1cont_remove_impl(&cont->end, last_idx);
  idx1cont_lazy_deinit_iter(cont, it);
}

Idx1it idx32it_cont_idx(const Idx1cont * cont, int32_t idx)
{
  return idx1cont_find_idx_impl(cont->base_idx, idx, Idx32chunk_elnum);
}

//Idx64cont definitions

enum Idx64chunk_en
{
  Idx64chunk_eltot = 13,
  Idx64chunk_elnum = Idx64chunk_eltot - 1
};

typedef struct Idx64chunk_t
{
  uint64_t data[Idx64chunk_eltot];
}
Idx64chunk;

//Idx64it methods family

uint64_t idx64it_getval(const Idx1it * it)
{
  const Idx64chunk * chunk = (const Idx64chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  return chunk->data[idx32chunk_getcount(it->chunk)];
}

void idx64it_setval(const Idx1it * it, uint64_t val)
{
  Idx64chunk * chunk = (Idx64chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  chunk->data[idx32chunk_getcount(it->chunk)] = val;
}

//Idx64cont methods family

void idx64cont_append(Idx1cont * cont, uint64_t val)
{
  idx1cont_lazy_init(cont, 64);
  idx64it_setval(&cont->end, val);
  idx1cont_append_impl(&cont->end, Idx64chunk_elnum, Idx32chunk_prev);
}

void idx64cont_remove(Idx1cont * cont, Idx1it * it)
{
  const uint32_t last_idx = idx32chunk_getnext(cont->end.chunk);
  idx1it_link2_decrement(&cont->end);
  idx64it_setval(it, idx64it_getval(&cont->end));
  idx1cont_remove_impl(&cont->end, last_idx);
  idx1cont_lazy_deinit_iter(cont, it);
}

Idx1it idx64it_cont_idx(const Idx1cont * cont, int32_t idx)
{
  return idx1cont_find_idx_impl(cont->base_idx, idx, Idx64chunk_elnum);
}

//Idx96cont definitions

enum Idx96chunk_en
{
  Idx96chunk_elnum = 8
};

typedef struct Idx96chunk_t
{
  Idx1cont data[Idx96chunk_elnum];
  uint64_t service;
}
Idx96chunk;

//Idx96it methods family

Idx1cont idx96it_getval(const Idx1it * it)
{
  const Idx96chunk * chunk = (const Idx96chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  return chunk->data[idx32chunk_getcount(it->chunk)];
}

void idx96it_setval(const Idx1it * it, Idx1cont val)
{
  Idx96chunk * chunk = (Idx96chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  chunk->data[idx32chunk_getcount(it->chunk)] = val;
}

//Idx96cont methods family

void idx96cont_append(Idx1cont * cont, Idx1cont val)
{
  idx1cont_lazy_init(cont, 96);
  idx96it_setval(&cont->end, val);
  idx1cont_append_impl(&cont->end, Idx96chunk_elnum, Idx32chunk_prev);
}

void idx96cont_remove(Idx1cont * cont, Idx1it * it)
{
  const uint32_t last_idx = idx32chunk_getnext(cont->end.chunk);
  idx1it_link2_decrement(&cont->end);
  idx96it_setval(it, idx96it_getval(&cont->end));
  idx1cont_remove_impl(&cont->end, last_idx);
  idx1cont_lazy_deinit_iter(cont, it);
}

Idx1it idx96it_cont_idx(const Idx1cont * cont, int32_t idx)
{
  return idx1cont_find_idx_impl(cont->base_idx, idx, Idx96chunk_elnum);
}

//Idx128cont definitions

enum Idx128chunk_en
{
  Idx128chunk_elnum = 6
};

typedef struct Idx128chunk_t
{
  Idx1range data[Idx128chunk_elnum];
  uint64_t service;
}
Idx128chunk;

//Idx128it methods family

Idx1range idx128it_getval(const Idx1it * it)
{
  const Idx128chunk * chunk = (const Idx128chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  return chunk->data[idx32chunk_getcount(it->chunk)];
}

void idx128it_setval(const Idx1it * it, Idx1range val)
{
  Idx128chunk * chunk = (Idx128chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  chunk->data[idx32chunk_getcount(it->chunk)] = val;
}

//Idx128cont methods family

void idx128cont_append(Idx1cont * cont, Idx1range val)
{
  idx1cont_lazy_init(cont, 128);
  idx128it_setval(&cont->end, val);
  idx1cont_append_impl(&cont->end, Idx128chunk_elnum, Idx32chunk_prev);
}

void idx128cont_remove(Idx1cont * cont, Idx1it * it)
{
  const uint32_t last_idx = idx32chunk_getnext(cont->end.chunk);
  idx1it_link2_decrement(&cont->end);
  idx128it_setval(it, idx128it_getval(&cont->end));
  idx1cont_remove_impl(&cont->end, last_idx);
  idx1cont_lazy_deinit_iter(cont, it);
}

Idx1it idx128it_cont_idx(const Idx1cont * cont, int32_t idx)
{
  return idx1cont_find_idx_impl(cont->base_idx, idx, Idx128chunk_elnum);
}

//"generic" stack methods family

bool idx1stack_empty(const Idx1cont * cont)
{
  return cont->end.count <= 0;
}

void idx1stack_pop(Idx1cont * cont)
{
  const uint32_t last_idx = idx32chunk_getnext(cont->end.chunk);
  idx1it_link2_decrement(&cont->end);
  idx1cont_remove_impl(&cont->end, last_idx);
  idx1cont_lazy_deinit(cont);
}

//Idx32cont stack methods family

void idx32stack_push(Idx1cont * cont, uint32_t val)
{
  idx32cont_append(cont, val);
}

void idx32stack_alter(const Idx1cont * cont, uint32_t val)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  idx32it_setval(&it, val);
}

uint32_t idx32stack_top(const Idx1cont * cont)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  return idx32it_getval(&it);
}

//Idx64cont stack methods family

void idx64stack_push(Idx1cont * cont, uint64_t val)
{
  idx64cont_append(cont, val);
}

void idx64stack_alter(const Idx1cont * cont, uint64_t val)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  idx64it_setval(&it, val);
}

uint64_t idx64stack_top(const Idx1cont * cont)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  return idx64it_getval(&it);
}

//Idx96cont stack methods family

void idx96stack_push(Idx1cont * cont, Idx1cont val)
{
  idx96cont_append(cont, val);
}

void idx96stack_alter(const Idx1cont * cont, Idx1cont val)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  idx96it_setval(&it, val);
}

Idx1cont idx96stack_top(const Idx1cont * cont)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  return idx96it_getval(&it);
}

//Idx128cont stack methods family

void idx128stack_push(Idx1cont * cont, Idx1range val)
{
  idx128cont_append(cont, val);
}

void idx128stack_alter(const Idx1cont * cont, Idx1range val)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  idx128it_setval(&it, val);
}

Idx1range idx128stack_top(const Idx1cont * cont)
{
  Idx1it it = cont->end;
  idx1it_link2_decrement(&it);
  return idx128it_getval(&it);
}

//"generic" queue methods family

bool idx1queue_empty(const Idx1range * range)
{
  return range->begin.count == range->end.count;
}

void idx1queue_pop_front(Idx1range * range)
{
  const uint32_t last_idx = idx32chunk_getnext(range->begin.chunk);
  idx1it_increment(&range->begin);
  idx1queue_remove_impl(&range->begin, &range->end, last_idx);
  idx1range_lazy_deinit(range);
}

void idx1queue_pop_back(Idx1range * range)
{
  const uint32_t last_idx = idx32chunk_getnext(range->end.chunk);
  idx1it_link2_decrement(&range->end);
  idx1cont_remove_impl(&range->end, last_idx);
  idx1range_lazy_deinit(range);
}

//Idx32range queue methods family

uint32_t idx32queue_front(const Idx1range * range)
{
  return idx32it_getval(&range->begin);
}

uint32_t idx32queue_back(const Idx1range * range)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  return idx32it_getval(&it);
}

void idx32queue_push_back(Idx1range * range, uint32_t val)
{
  idx1range_lazy_init(range);
  idx32it_setval(&range->end, val);
  idx1cont_append_impl(&range->end, Idx32chunk_elnum, Idx32chunk_prev);
}

void idx32queue_push_front(Idx1range * range, uint32_t val)
{
  idx1range_lazy_init_full(range, Idx32chunk_elnum);
  idx1range_prepend_impl(&range->begin, &range->end, Idx32chunk_elnum);
  idx32it_setval(&range->begin, val);
}

void idx32queue_alter_back(const Idx1range * range, uint32_t val)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  idx32it_setval(&it, val);
}

void idx32queue_alter_front(const Idx1range * range, uint32_t val)
{
  idx32it_setval(&range->begin, val);
}

//Idx1it idx32it_queue_idx(const Idx1range * range, int32_t idx)
//{
//  return idx1range_find_idx_impl(&range->begin, idx, Idx32chunk_elnum);
//}

//Idx64range queue methods family

uint64_t idx64queue_front(const Idx1range * range)
{
  return idx64it_getval(&range->begin);
}

uint64_t idx64queue_back(const Idx1range * range)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  return idx64it_getval(&it);
}

void idx64queue_push_back(Idx1range * range, uint64_t val)
{
  idx1range_lazy_init(range);
  idx64it_setval(&range->end, val);
  idx1cont_append_impl(&range->end, Idx64chunk_elnum, Idx32chunk_prev);
}

void idx64queue_push_front(Idx1range * range, uint64_t val)
{
  idx1range_lazy_init_full(range, Idx64chunk_elnum);
  idx1range_prepend_impl(&range->begin, &range->end, Idx64chunk_elnum);
  idx64it_setval(&range->begin, val);
}

void idx64queue_alter_back(const Idx1range * range, uint64_t val)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  idx64it_setval(&it, val);
}

void idx64queue_alter_front(const Idx1range * range, uint64_t val)
{
  idx64it_setval(&range->begin, val);
}

//Idx1it idx64it_queue_idx(const Idx1range * range, int32_t idx)
//{
//  return idx1range_find_idx_impl(&range->begin, idx, Idx64chunk_elnum);
//}

//Idx96range queue methods family

Idx1cont idx96queue_front(const Idx1range * range)
{
  return idx96it_getval(&range->begin);
}

Idx1cont idx96queue_back(const Idx1range * range)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  return idx96it_getval(&it);
}

void idx96queue_push_back(Idx1range * range, Idx1cont val)
{
  idx1range_lazy_init(range);
  idx96it_setval(&range->end, val);
  idx1cont_append_impl(&range->end, Idx96chunk_elnum, Idx32chunk_prev);
}

void idx96queue_push_front(Idx1range * range, Idx1cont val)
{
  idx1range_lazy_init_full(range, Idx96chunk_elnum);
  idx1range_prepend_impl(&range->begin, &range->end, Idx96chunk_elnum);
  idx96it_setval(&range->begin, val);
}

void idx96queue_alter_back(const Idx1range * range, Idx1cont val)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  idx96it_setval(&it, val);
}

void idx96queue_alter_front(const Idx1range * range, Idx1cont val)
{
  idx96it_setval(&range->begin, val);
}

//Idx1it idx96it_queue_idx(const Idx1range * range, int32_t idx)
//{
//  return idx1range_find_idx_impl(&range->begin, idx, Idx96chunk_elnum);
//}

//Idx128range queue methods family

Idx1range idx128queue_front(const Idx1range * range)
{
  return idx128it_getval(&range->begin);
}

Idx1range idx128queue_back(const Idx1range * range)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  return idx128it_getval(&it);
}

void idx128queue_push_back(Idx1range * range, Idx1range val)
{
  idx1range_lazy_init(range);
  idx128it_setval(&range->end, val);
  idx1cont_append_impl(&range->end, Idx128chunk_elnum, Idx32chunk_prev);
}

void idx128queue_push_front(Idx1range * range, Idx1range val)
{
  idx1range_lazy_init_full(range, Idx128chunk_elnum);
  idx1range_prepend_impl(&range->begin, &range->end, Idx128chunk_elnum);
  idx128it_setval(&range->begin, val);
}

void idx128queue_alter_back(const Idx1range * range, Idx1range val)
{
  Idx1it it = range->end;
  idx1it_link2_decrement(&it);
  idx128it_setval(&it, val);
}

void idx128queue_alter_front(const Idx1range * range, Idx1range val)
{
  idx128it_setval(&range->begin, val);
}

//Idx1it idx128it_queue_idx(const Idx1range * range, int32_t idx)
//{
//  return idx1range_find_idx_impl(&range->begin, idx, Idx128chunk_elnum);
//}

//Idx160cont definitions

enum Idx160chunk_en
{
  Idx160chunk_elnum = 5
};

typedef struct Idx160chunk_t
{
  Idx1nonsense1 data[Idx160chunk_elnum];
  uint32_t service;
}
Idx160chunk;

//Idx160it methods family

Idx1nonsense1 idx160it_getval(const Idx1it * it)
{
  const Idx160chunk * chunk = (const Idx160chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  return chunk->data[idx32chunk_getcount(it->chunk)];
}

void idx160it_setval(const Idx1it * it, Idx1nonsense1 val)
{
  Idx160chunk * chunk = (Idx160chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  chunk->data[idx32chunk_getcount(it->chunk)] = val;
}

//Idx160cont methods family

void idx160cont_append(Idx1cont * cont, Idx1nonsense1 val)
{
  idx1cont_lazy_init(cont, 160);
  idx160it_setval(&cont->end, val);
  idx1cont_append_impl(&cont->end, Idx160chunk_elnum, 0);
}

void idx160cont_remove(Idx1cont * cont, Idx1it * it)
{
  const uint32_t last_idx = idx32chunk_getnext(cont->end.chunk);
  idx1it_link1_decrement(&cont->end, cont->base_idx, Idx160chunk_elnum);
  idx160it_setval(it, idx160it_getval(&cont->end));
  idx1cont_remove_impl(&cont->end, last_idx);
  idx1cont_lazy_deinit_iter(cont, it);
}

Idx1it idx160it_cont_idx(const Idx1cont * cont, int32_t idx)
{
  return idx1cont_find_idx_impl(cont->base_idx, idx, Idx160chunk_elnum);
}

//Idx192cont definitions

enum Idx192chunk_en
{
  Idx192chunk_elnum = 4
};

typedef struct Idx192chunk_t
{
  Idx1nonsense2 data[Idx192chunk_elnum];
  uint64_t service;
}
Idx192chunk;

//Idx192it methods family

Idx1nonsense2 idx192it_getval(const Idx1it * it)
{
  const Idx192chunk * chunk = (const Idx192chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  return chunk->data[idx32chunk_getcount(it->chunk)];
}

void idx192it_setval(const Idx1it * it, Idx1nonsense2 val)
{
  Idx192chunk * chunk = (Idx192chunk *)&idx32pool[idx32chunk_getnext(it->chunk)];
  chunk->data[idx32chunk_getcount(it->chunk)] = val;
}

//Idx192cont methods family

void idx192cont_append(Idx1cont * cont, Idx1nonsense2 val)
{
  idx1cont_lazy_init(cont, 192);
  idx192it_setval(&cont->end, val);
  idx1cont_append_impl(&cont->end, Idx192chunk_elnum, Idx32chunk_prev);
}

void idx192cont_remove(Idx1cont * cont, Idx1it * it)
{
  const uint32_t last_idx = idx32chunk_getnext(cont->end.chunk);
  idx1it_link2_decrement(&cont->end);
  idx192it_setval(it, idx192it_getval(&cont->end));
  idx1cont_remove_impl(&cont->end, last_idx);
  idx1cont_lazy_deinit_iter(cont, it);
}

Idx1it idx192it_cont_idx(const Idx1cont * cont, int32_t idx)
{
  return idx1cont_find_idx_impl(cont->base_idx, idx, Idx192chunk_elnum);
}
