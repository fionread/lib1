
#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef struct Idx1it_t
{
  uint32_t chunk;
  int32_t count;
}
Idx1it;

typedef struct Idx1cont_t
{
  uint32_t base_idx;
  Idx1it end;
}
Idx1cont;

typedef struct Idx1range_t
{
  Idx1it begin;
  Idx1it end;
}
Idx1range;


//count may not ecxeed (1 << 24) three bytes for index
void idx1pool_reserve(uint32_t count);

//delete "generic" container/queue
void idx1cont_release(Idx1cont * cont);
void idx1queue_release(Idx1range * range);

//zero "generic" container prerequisite for *_append()
Idx1cont idx1cont_zero();
Idx1range idx1range_zero();
Idx1it idx1it_zero();

//"generic" container deep copy
Idx1cont idx1cont_copy(Idx1cont * from);
Idx1range idx1range_copy(Idx1range * from);

//"generic" container bits count get/set
uint8_t idx1cont_get_bits(const Idx1cont * cont);
void idx1cont_set_bits(Idx1cont * cont, uint8_t bits);

//"generic" iterator methods family
Idx1it idx1it_begin(const Idx1cont * cont);
bool idx1it_valid(const Idx1it * it);
void idx1it_increment(Idx1it * it);
//idx1it_decrement() is not implemented due to reasons:
//1. Idx160cont is single linked
//2. idx1queue_push_front() should be reimplemented then

//Idx32it methods family
uint32_t idx32it_getval(const Idx1it * it);
void idx32it_setval(const Idx1it * it, uint32_t val);

//Idx32cont methods family
void idx32cont_append(Idx1cont * cont, uint32_t val);
void idx32cont_remove(Idx1cont * cont, Idx1it * it);
Idx1it idx32it_cont_idx(const Idx1cont * cont, int32_t idx);

//Idx64it methods family
uint64_t idx64it_getval(const Idx1it * it);
void idx64it_setval(const Idx1it * it, uint64_t val);

//Idx64cont methods family
void idx64cont_append(Idx1cont * cont, uint64_t val);
void idx64cont_remove(Idx1cont * cont, Idx1it * it);
Idx1it idx64it_cont_idx(const Idx1cont * cont, int32_t idx);

//Idx96it methods family
Idx1cont idx96it_getval(const Idx1it * it);
void idx96it_setval(const Idx1it * it, Idx1cont val);

//Idx96cont methods family
void idx96cont_append(Idx1cont * cont, Idx1cont val);
void idx96cont_remove(Idx1cont * cont, Idx1it * it);
Idx1it idx96it_cont_idx(const Idx1cont * cont, int32_t idx);

//Idx128it methods family
Idx1range idx128it_getval(const Idx1it * it);
void idx128it_setval(const Idx1it * it, Idx1range val);

//Idx128cont methods family
void idx128cont_append(Idx1cont * cont, Idx1range val);
void idx128cont_remove(Idx1cont * cont, Idx1it * it);
Idx1it idx128it_cont_idx(const Idx1cont * cont, int32_t idx);

//"generic" stack methods family
bool idx1stack_empty(const Idx1cont * cont);
void idx1stack_pop(Idx1cont * cont);

//Idx32cont stack methods family
void idx32stack_push(Idx1cont * cont, uint32_t val);
void idx32stack_alter(const Idx1cont * cont, uint32_t val);
uint32_t idx32stack_top(const Idx1cont * cont);

//Idx64cont stack methods family
void idx64stack_push(Idx1cont * cont, uint64_t val);
void idx64stack_alter(const Idx1cont * cont, uint64_t val);
uint64_t idx64stack_top(const Idx1cont * cont);

//Idx96cont stack methods family
void idx96stack_push(Idx1cont * cont, Idx1cont val);
void idx96stack_alter(const Idx1cont * cont, Idx1cont val);
Idx1cont idx96stack_top(const Idx1cont * cont);

//Idx128cont stack methods family
void idx128stack_push(Idx1cont * cont, Idx1range val);
void idx128stack_alter(const Idx1cont * cont, Idx1range val);
Idx1range idx128stack_top(const Idx1cont * cont);

//"generic" queue methods family
bool idx1queue_empty(const Idx1range * range);
void idx1queue_pop_front(Idx1range * range);
void idx1queue_pop_back(Idx1range * range);

//Idx32range queue methods family
uint32_t idx32queue_front(const Idx1range * range);
uint32_t idx32queue_back(const Idx1range * range);
void idx32queue_push_back(Idx1range * range, uint32_t val);
void idx32queue_push_front(Idx1range * range, uint32_t val);
void idx32queue_alter_back(const Idx1range * range, uint32_t val);
void idx32queue_alter_front(const Idx1range * range, uint32_t val);

//Idx64range queue methods family
uint64_t idx64queue_front(const Idx1range * range);
uint64_t idx64queue_back(const Idx1range * range);
void idx64queue_push_back(Idx1range * range, uint64_t val);
void idx64queue_push_front(Idx1range * range, uint64_t val);
void idx64queue_alter_back(const Idx1range * range, uint64_t val);
void idx64queue_alter_front(const Idx1range * range, uint64_t val);

//Idx96range queue methods family
Idx1cont idx96queue_front(const Idx1range * range);
Idx1cont idx96queue_back(const Idx1range * range);
void idx96queue_push_back(Idx1range * range, Idx1cont val);
void idx96queue_push_front(Idx1range * range, Idx1cont val);
void idx96queue_alter_back(const Idx1range * range, Idx1cont val);
void idx96queue_alter_front(const Idx1range * range, Idx1cont val);

//Idx128range queue methods family
Idx1range idx128queue_front(const Idx1range * range);
Idx1range idx128queue_back(const Idx1range * range);
void idx128queue_push_back(Idx1range * range, Idx1range val);
void idx128queue_push_front(Idx1range * range, Idx1range val);
void idx128queue_alter_back(const Idx1range * range, Idx1range val);
void idx128queue_alter_front(const Idx1range * range, Idx1range val);
