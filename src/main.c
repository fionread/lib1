
#include "gph1algo.h"
#include "hsh1mapf.h"
#include "pri1que.h"
#include "idx1internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

void idx32test(Idx1cont * some32)
{
  Idx1it its32;
  uint32_t is32 = 0;

  if (some32->base_idx & 0xffffff)
  {
    for (its32 = idx1it_begin(some32); idx1it_valid(&its32); idx1it_increment(&its32))
    {
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is32, its32.chunk >> 8, its32.chunk & 0xff, its32.count, idx32it_getval(&its32));
      ++is32;
    }
    assert(is32 == some32->end.count);
  }
  //itb = idx1it_begin(0);
  //printf("  #: garbage val = %d\n", idx32it_getval(&itb));
  //assert(!idx1it_valid(&itb));
}

void rng32test(Idx1range * range)
{
  Idx1it its32;
  uint32_t is32 = 0;

  if (range->begin.chunk)
  {
    for (its32 = range->begin; idx1it_valid(&its32); idx1it_increment(&its32))
    {
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is32, its32.chunk >> 8, its32.chunk & 0xff, its32.count, idx32it_getval(&its32));
      ++is32;
    }
    assert(is32 == range->end.count);
  }
}

void idx64test(Idx1cont * some64)
{
  Idx1it its64;
  uint32_t is64 = 0;

  if (some64->base_idx & 0xffffff)
  {
    for (its64 = idx1it_begin(some64); idx1it_valid(&its64); idx1it_increment(&its64))
    {
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is64, its64.chunk >> 8, its64.chunk & 0xff, its64.count, idx64it_getval(&its64) >> 32);
      ++is64;
    }
    assert(is64 == some64->end.count);
  }
}

void rng64test(Idx1range * range)
{
  Idx1it its64;
  uint32_t is64 = 0;

  if (range->begin.chunk)
  {
    for (its64 = range->begin; idx1it_valid(&its64); idx1it_increment(&its64))
    {
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is64, its64.chunk >> 8, its64.chunk & 0xff, its64.count, idx64it_getval(&its64));
      ++is64;
    }
    assert(is64 == range->end.count);
  }
}

void idx96test(Idx1cont * some)
{
  Idx1it its;
  uint32_t is = 0;

  if (some->base_idx & 0xffffff)
  {
    for (its = idx1it_begin(some); idx1it_valid(&its); idx1it_increment(&its))
    {
      Idx1cont val = idx96it_getval(&its);
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is, its.chunk >> 8, its.chunk & 0xff, its.count, val.base_idx);
      assert(val.base_idx == val.end.chunk);
      assert(val.base_idx == val.end.count);
      ++is;
    }
    assert(is == some->end.count);
  }
}

void rng96test(Idx1range * range)
{
  Idx1it its96;
  uint32_t is96 = 0;

  if (range->begin.chunk)
  {
    for (its96 = range->begin; idx1it_valid(&its96); idx1it_increment(&its96))
    {
      Idx1cont val = idx96it_getval(&its96);
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is96, its96.chunk >> 8, its96.chunk & 0xff, its96.count, val.base_idx);
      ++is96;
    }
    assert(is96 == range->end.count);
  }
}

void idx128test(Idx1cont * some)
{
  Idx1it its;
  uint32_t is = 0;

  if (some->base_idx & 0xffffff)
  {
    for (its = idx1it_begin(some); idx1it_valid(&its); idx1it_increment(&its))
    {
      Idx1range val = idx128it_getval(&its);
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is, its.chunk >> 8, its.chunk & 0xff, its.count, val.begin.chunk);
      assert(val.begin.chunk == val.end.chunk);
      assert(val.begin.count == val.end.count);
      ++is;
    }
    assert(is == some->end.count);
  }
}

void rng128test(Idx1range * range)
{
  Idx1it its128;
  uint32_t is128 = 0;

  if (range->begin.chunk)
  {
    for (its128 = range->begin; idx1it_valid(&its128); idx1it_increment(&its128))
    {
      Idx1range val = idx128it_getval(&its128);
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is128, its128.chunk >> 8, its128.chunk & 0xff, its128.count, val.begin.chunk);
      ++is128;
    }
    assert(is128 == range->end.count);
  }
}

void idx160test(Idx1cont * some)
{
  Idx1it its;
  uint32_t is = 0;

  if (some->base_idx & 0xffffff)
  {
    for (its = idx1it_begin(some); idx1it_valid(&its); idx1it_increment(&its))
    {
      Idx1nonsense1 val = idx160it_getval(&its);
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is, its.chunk >> 8, its.chunk & 0xff, its.count, val.end.chunk);
      assert(val.cont.end.chunk == val.end.chunk);
      assert(val.cont.end.count == val.end.count);
      ++is;
    }
    assert(is == some->end.count);
  }
}

void idx192test(Idx1cont * some)
{
  Idx1it its;
  uint32_t is = 0;

  if (some->base_idx & 0xffffff)
  {
    for (its = idx1it_begin(some); idx1it_valid(&its); idx1it_increment(&its))
    {
      Idx1nonsense2 val = idx192it_getval(&its);
      printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
        is, its.chunk >> 8, its.chunk & 0xff, its.count, val.end.chunk);
      assert(val.range.begin.chunk == val.end.chunk);
      assert(val.range.begin.count == val.end.count);
      assert(val.range.end.chunk == val.end.chunk);
      assert(val.range.end.count == val.end.count);
      ++is;
    }
    assert(is == some->end.count);
  }
}

void idx32test_suite()
{
  int i;
  Idx1it its32, itr;
  Idx1cont some32 = idx1cont_zero();
  Idx1range range = idx1range_zero();
  Idx1cont other;

  printf("init: some32 base = %d, count = %d, bits = %d\n",
    some32.base_idx & 0xffffff, some32.end.count, idx1cont_get_bits(&some32));
  idx32test(&some32);

  for (i = 0; i < 9; ++i)
  {
    uint32_t val32 = (i + 1) * 100;
    idx32cont_append(&some32, val32);
    printf("append: val %d, count = %d\n", val32, some32.end.count);
    idx32test(&some32);
  }
  
  printf("copy: count = %d\n", some32.end.count);
  other = idx1cont_copy(&some32);
  idx32test(&other);
  idx1cont_release(&other);
  
  for (i = 0; i < 8; ++i)
  {
    Idx1it it32 = idx1it_begin(&some32);
    uint32_t val32 = idx32it_getval(&it32);
    idx32cont_remove(&some32, &it32);
    printf("remove: val %d, count = %d\n", val32, some32.end.count);
    idx32test(&some32);
  }

  for (i = 0; i < 9; ++i)
  {
    uint32_t val32 = (i + 1) * 1000;
    idx32cont_append(&some32, val32);
    printf("append: val %d, count = %d\n", val32, some32.end.count);
    idx32test(&some32);
  }

  //its32 = idx1it_begin(&some32);
  //while (idx1it_valid(&its32))
  //{
  //  uint32_t val32;
  //  val32 = idx32it_getval(&its32);
  //  idx32cont_remove(&some32, &its32);
  //  printf("remove: val %d, count = %d\n", val32, some32.end.count);
  //  idx32test(&some32);
  //}
  while(!idx1stack_empty(&some32))
  {
    uint32_t val32 = idx32stack_top(&some32);
    idx1stack_pop(&some32);
    printf("remove: val %d, count = %d\n", val32, some32.end.count);
    idx32test(&some32);
  }

  for (i = 0; i < 1; ++i)
  {
    uint32_t val32 = (i + 1) * 100;
    idx32cont_append(&some32, val32);
    printf("append: val %d, count = %d\n", val32, some32.end.count);
    idx32test(&some32);
  }

  for (i = 0; i < 5; ++i)
  {
    uint32_t val32 = (i + 1) * 10000;
    idx32cont_append(&some32, val32);
    printf("append: val %d, count = %d\n", val32, some32.end.count);
    idx32test(&some32);
  }

  i = 5;
  itr = idx32it_cont_idx(&some32, i);
  assert(idx1it_valid(&itr));
  idx32cont_remove(&some32, &itr);
  printf("remove: %d element, count = %d\n", i, some32.end.count);
  idx32test(&some32);

  printf("erase: all\n");
  idx1cont_release(&some32);
  idx32test(&some32);
  printf("init: some32 base = %d, count = %d, bits = %d\n",
    some32.base_idx & 0xffffff, some32.end.count, idx1cont_get_bits(&some32));

  for (i = 0; i < 9; ++i)
  {
    uint32_t val32 = (i + 1) * 100000;
    if (i & 1)
    {
      idx32queue_push_back(&range, val32);
      printf("append: val %d, count = %d\n", val32, range.end.count);
    }
    else
    {
      idx32queue_push_front(&range, val32);
      printf("prepend: val %d, count = %d\n", val32, range.end.count);
    }
    rng32test(&range);
  }
  while (idx1it_valid(&range.begin))
  {
    uint32_t val32;
    val32 = idx32queue_back(&range);
    //idx1queue_pop_front(&range);
    idx1queue_pop_back(&range);
    printf("remove: val %d, count = %d\n", val32, range.end.count);
    rng32test(&range);
  }
  //idx1queue_release(&range);
}

void idx64test_suite()
{
  int i;
  Idx1it its64, itr;
  Idx1cont some64 = idx1cont_zero();
  Idx1range range = idx1range_zero();
  Idx1cont other;

  printf("init: some64 base = %d, count = %d, bits = %d\n",
    some64.base_idx & 0xffffff, some64.end.count, idx1cont_get_bits(&some64));
  idx64test(&some64);

  for (i = 0; i < 9; ++i)
  {
    uint64_t val64 = (i + 1) * 100;
    idx64cont_append(&some64, val64 << 32);
    printf("append: val %d, count = %d\n", val64, some64.end.count);
    idx64test(&some64);
  }

  printf("copy: count = %d\n", some64.end.count);
  other = idx1cont_copy(&some64);
  idx64test(&other);
  idx1cont_release(&other);

  for (i = 0; i < 8; ++i)
  {
    Idx1it it64 = idx1it_begin(&some64);
    uint64_t val64 = idx64it_getval(&it64);
    idx64cont_remove(&some64, &it64);
    printf("remove: val %d, count = %d\n", val64 >> 32, some64.end.count);
    idx64test(&some64);
  }

  for (i = 0; i < 9; ++i)
  {
    uint64_t val64 = (i + 1) * 1000;
    idx64cont_append(&some64, val64 << 32);
    printf("append: val %d, count = %d\n", val64, some64.end.count);
    idx64test(&some64);
  }

  //its64 = idx1it_begin(&some64);
  //while (idx1it_valid(&its64))
  //{
  //  uint64_t val64;
  //  val64 = idx64it_getval(&its64);
  //  idx64cont_remove(&some64, &its64);
  //  printf("remove: val %d, count = %d\n", val64 >> 32, some64.end.count);
  //  idx64test(&some64);
  //}
  while(!idx1stack_empty(&some64))
  {
    uint64_t val64 = idx64stack_top(&some64);
    idx1stack_pop(&some64);
    printf("remove: val %d, count = %d\n", val64 >> 32, some64.end.count);
    idx64test(&some64);
  }

  for (i = 0; i < 1; ++i)
  {
    uint64_t val64 = (i + 1) * 100;
    idx64cont_append(&some64, val64 << 32);
    printf("append: val %d, count = %d\n", val64, some64.end.count);
    idx64test(&some64);
  }

  for (i = 0; i < 5; ++i)
  {
    uint64_t val64 = (i + 1) * 10000;
    idx64cont_append(&some64, val64 << 32);
    printf("append: val %d, count = %d\n", val64, some64.end.count);
    idx64test(&some64);
  }

  i = 5;
  itr = idx64it_cont_idx(&some64, i);
  assert(idx1it_valid(&itr));
  idx64cont_remove(&some64, &itr);
  printf("remove: %d element, count = %d\n", i, some64.end.count);
  idx64test(&some64);

  printf("erase: all\n");
  idx1cont_release(&some64);
  idx64test(&some64); 
  printf("init: some64 base = %d, count = %d, bits = %d\n",
    some64.base_idx & 0xffffff, some64.end.count, idx1cont_get_bits(&some64));
    
  for (i = 0; i < 9; ++i)
  {
    uint64_t val64 = (i + 1) * 100000;
    idx64queue_push_back(&range, val64);
    printf("append: val %d, count = %d\n", val64, range.end.count);
    rng64test(&range);
  }
  while (idx1it_valid(&range.begin))
  {
    uint64_t val64;
    val64 = idx64it_getval(&range.begin);
    idx1queue_pop_front(&range);
    printf("remove: val %d, count = %d\n", val64, range.end.count);
    rng64test(&range);
  }
  //idx1queue_release(&range);
}

void idx96test_suite()
{
  int i;
  Idx1it its, itr;
  Idx1cont some = idx1cont_zero();
  Idx1range range = idx1range_zero();
  Idx1cont other;

  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));
  idx96test(&some);

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1cont cont = { .base_idx = val, .end = { .chunk = val, .count = val } };
    idx96cont_append(&some, cont);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx96test(&some);
  }

  printf("copy: count = %d\n", some.end.count);
  other = idx1cont_copy(&some);
  idx96test(&other);
  idx1cont_release(&other);

  for (i = 0; i < 8; ++i)
  {
    Idx1it it = idx1it_begin(&some);
    Idx1cont cont = idx96it_getval(&it);
    idx96cont_remove(&some, &it);
    printf("remove: val %d, count = %d\n", cont.base_idx, some.end.count);
    idx96test(&some);
  }

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 1000;
    Idx1cont cont = { .base_idx = val, .end = { .chunk = val, .count = val } };
    idx96cont_append(&some, cont);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx96test(&some);
  }

  //its = idx1it_begin(&some);
  //while (idx1it_valid(&its))
  //{
  //  Idx1cont cont = idx96it_getval(&its);
  //  idx96cont_remove(&some, &its);
  //  printf("remove: val %d, count = %d\n", cont.base_idx, some.end.count);
  //  idx96test(&some);
  //}
  while(!idx1stack_empty(&some))
  {
    Idx1cont cont = idx96stack_top(&some);
    idx1stack_pop(&some);
    printf("remove: val %d, count = %d\n", cont.base_idx, some.end.count);
    idx96test(&some);
  }

  for (i = 0; i < 1; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1cont cont = { .base_idx = val, .end = { .chunk = val, .count = val } };
    idx96cont_append(&some, cont);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx96test(&some);
  }

  for (i = 0; i < 5; ++i)
  {
    uint32_t val = (i + 1) * 10000;
    Idx1cont cont = { .base_idx = val, .end = { .chunk = val, .count = val } };
    idx96cont_append(&some, cont);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx96test(&some);
  }

  i = 5;
  itr = idx96it_cont_idx(&some, i);
  assert(idx1it_valid(&itr));
  idx96cont_remove(&some, &itr);
  printf("remove: %d element, count = %d\n", i, some.end.count);
  idx96test(&some);

  printf("erase: all\n");
  idx1cont_release(&some);
  idx96test(&some); 
  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));
  
  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100000;
    Idx1cont cont = { .base_idx = val, .end = { .chunk = val, .count = val } };
    idx96queue_push_back(&range, cont);
    printf("append: val %d, count = %d\n", val, range.end.count);
    rng96test(&range);
  }
  while (idx1it_valid(&range.begin))
  {
    Idx1cont cont;
    cont = idx96it_getval(&range.begin);
    idx1queue_pop_front(&range);
    printf("remove: val %d, count = %d\n", cont.base_idx, range.end.count);
    rng96test(&range);
  }
  //idx1queue_release(&range);
}

void idx128test_suite()
{
  int i;
  Idx1it its, itr;
  Idx1cont some = idx1cont_zero();
  Idx1range range = idx1range_zero();
  Idx1cont other;
  Idx1range range_copy;

  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));
  idx128test(&some);

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1range range = { .begin = { .chunk = val, .count = val }, .end = { .chunk = val, .count = val } };
    idx128cont_append(&some, range);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx128test(&some);
  }

  printf("copy: count = %d\n", some.end.count);
  other = idx1cont_copy(&some);
  idx128test(&other);
  idx1cont_release(&other);

  for (i = 0; i < 8; ++i)
  {
    Idx1it it = idx1it_begin(&some);
    Idx1range range = idx128it_getval(&it);
    idx128cont_remove(&some, &it);
    printf("remove: val %d, count = %d\n", range.begin.count, some.end.count);
    idx128test(&some);
  }

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 1000;
    Idx1range range = { .begin = { .chunk = val, .count = val }, .end = { .chunk = val, .count = val } };
    idx128cont_append(&some, range);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx128test(&some);
  }

  //its = idx1it_begin(&some);
  //while (idx1it_valid(&its))
  //{
  //  Idx1range range = idx128it_getval(&its);
  //  idx128cont_remove(&some, &its);
  //  printf("remove: val %d, count = %d\n", range.begin.count, some.end.count);
  //  idx128test(&some);
  //}
  while(!idx1stack_empty(&some))
  {
    Idx1range range = idx128stack_top(&some);
    idx1stack_pop(&some);
    printf("remove: val %d, count = %d\n", range.begin.count, some.end.count);
    idx128test(&some);
  }

  for (i = 0; i < 1; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1range range = { .begin = { .chunk = val, .count = val }, .end = { .chunk = val, .count = val } };
    idx128cont_append(&some, range);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx128test(&some);
  }

  for (i = 0; i < 5; ++i)
  {
    uint32_t val = (i + 1) * 10000;
    Idx1range range = { .begin = { .chunk = val, .count = val }, .end = { .chunk = val, .count = val } };
    idx128cont_append(&some, range);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx128test(&some);
  }

  i = 5;
  itr = idx128it_cont_idx(&some, i);
  assert(idx1it_valid(&itr));
  idx128cont_remove(&some, &itr);
  printf("remove: %d element, count = %d\n", i, some.end.count);
  idx128test(&some);

  printf("erase: all\n");
  idx1cont_release(&some);
  idx128test(&some); 
  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100000;
    Idx1range rng = { .begin = { .chunk = val, .count = val }, .end = { .chunk = val, .count = val } };
    idx128queue_push_back(&range, rng);
    printf("append: val %d, count = %d\n", val, range.end.count);
    rng128test(&range);
  }
  range_copy = idx1range_copy(&range);

  i = 0;
  while (idx1it_valid(&range.begin))
  {
    Idx1range rng = idx1range_zero();
    if (i & 1)
    {
      rng = idx128queue_front(&range);
      idx1queue_pop_front(&range);
    }
    else
    {
      rng = idx128queue_back(&range);
      idx1queue_pop_back(&range);
    }

    printf("remove: val %d, count = %d\n", rng.begin.chunk, range.end.count);
    //its = idx128it_queue_idx(&range, 2);
    //printf("  idx2 to it { %d, %d } %d\n", its.chunk >> 8, its.chunk & 0xff, its.count);
    //if (idx1it_valid(&its))
    //{
    //  Idx1range val128 = idx128it_getval(&its);
    //  printf("    val = %d\n", val128.begin.count);
    //}
    ++i;
    rng128test(&range);
  }
  
  printf("copy: range count = %d\n", range_copy.end.count);
  rng128test(&range_copy);
  idx1queue_release(&range_copy);
}

void idx160test_suite()
{
  int i;
  Idx1it its, itr;
  Idx1cont some;

  some = idx1cont_zero();
  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));
  idx160test(&some);

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense1 nons = { .cont = { .base_idx = val, .end = end }, .end = end };
    idx160cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx160test(&some);
  }

  for (i = 0; i < 8; ++i)
  {
    Idx1it it = idx1it_begin(&some);
    Idx1nonsense1 nons = idx160it_getval(&it);
    idx160cont_remove(&some, &it);
    printf("remove: val %d, count = %d\n", nons.end.count, some.end.count);
    idx160test(&some);
  }

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 1000;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense1 nons = { .cont = { .base_idx = val, .end = end }, .end = end };
    idx160cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx160test(&some);
  }

  its = idx1it_begin(&some);
  while (idx1it_valid(&its))
  {
    Idx1nonsense1 nons = idx160it_getval(&its);
    idx160cont_remove(&some, &its);
    printf("remove: val %d, count = %d\n", nons.end.count, some.end.count);
    idx160test(&some);
  }
  //while(!idx1stack_empty(&some))
  //{
  //  Idx1nonsense1 nons = idx160stack_top(&some);
  //  idx1stack_pop(&some);
  //  printf("remove: val %d, count = %d\n", nons.end.count, some.end.count);
  //  idx160test(&some);
  //}

  for (i = 0; i < 1; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense1 nons = { .cont = { .base_idx = val, .end = end }, .end = end };
    idx160cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx160test(&some);
  }

  for (i = 0; i < 5; ++i)
  {
    uint32_t val = (i + 1) * 10000;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense1 nons = { .cont = { .base_idx = val, .end = end }, .end = end };
    idx160cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx160test(&some);
  }

  i = 5;
  itr = idx160it_cont_idx(&some, i);
  assert(idx1it_valid(&itr));
  idx160cont_remove(&some, &itr);
  printf("remove: %d element, count = %d\n", i, some.end.count);
  idx160test(&some);

  printf("erase: all\n");
  idx1cont_release(&some);
  idx160test(&some);
 
  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));
  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100000;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense1 nons = { .cont = { .base_idx = val, .end = end }, .end = end };
    idx160cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx160test(&some);
  }
  idx1cont_release(&some);
}

void idx192test_suite()
{
  int i;
  Idx1it its, itr;
  Idx1cont some;

  some = idx1cont_zero();
  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));
  idx192test(&some);

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense2 nons = { .range = { .begin = end, .end = end }, .end = end };
    idx192cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx192test(&some);
  }

  for (i = 0; i < 8; ++i)
  {
    Idx1it it = idx1it_begin(&some);
    Idx1nonsense2 nons = idx192it_getval(&it);
    idx192cont_remove(&some, &it);
    printf("remove: val %d, count = %d\n", nons.end.count, some.end.count);
    idx192test(&some);
  }

  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 1000;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense2 nons = { .range = { .begin = end, .end = end }, .end = end };
    idx192cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx192test(&some);
  }

  its = idx1it_begin(&some);
  while (idx1it_valid(&its))
  {
    Idx1nonsense2 nons = idx192it_getval(&its);
    idx192cont_remove(&some, &its);
    printf("remove: val %d, count = %d\n", nons.end.count, some.end.count);
    idx192test(&some);
  }
  //while(!idx1stack_empty(&some))
  //{
  //  Idx1nonsense2 nons = idx192stack_top(&some);
  //  idx1stack_pop(&some);
  //  printf("remove: val %d, count = %d\n", nons.end.count, some.end.count);
  //  idx192test(&some);
  //}

  for (i = 0; i < 1; ++i)
  {
    uint32_t val = (i + 1) * 100;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense2 nons = { .range = { .begin = end, .end = end }, .end = end };
    idx192cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx192test(&some);
  }

  for (i = 0; i < 5; ++i)
  {
    uint32_t val = (i + 1) * 10000;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense2 nons = { .range = { .begin = end, .end = end }, .end = end };
    idx192cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx192test(&some);
  }

  i = 5;
  itr = idx192it_cont_idx(&some, i);
  assert(idx1it_valid(&itr));
  idx192cont_remove(&some, &itr);
  printf("remove: %d element, count = %d\n", i, some.end.count);
  idx192test(&some);

  printf("erase: all\n");
  idx1cont_release(&some);
  idx192test(&some);
 
  printf("init: some base = %d, count = %d, bits = %d\n",
    some.base_idx & 0xffffff, some.end.count, idx1cont_get_bits(&some));
  for (i = 0; i < 9; ++i)
  {
    uint32_t val = (i + 1) * 100000;
    Idx1it end = { .chunk = val, .count = val };
    Idx1nonsense2 nons = { .range = { .begin = end, .end = end }, .end = end };
    idx192cont_append(&some, nons);
    printf("append: val %d, count = %d\n", val, some.end.count);
    idx192test(&some);
  }
  idx1cont_release(&some);
}

void hsh32test(Hsh1cont * cont)
{
  uint32_t n;
 
  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its32;
      uint32_t is32 = 0;
      for (its32 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its32); idx1it_increment(&its32))
      {
        printf("  %d: it = { { %d, %d }, %d }, val = %d\n",
          is32, its32.chunk >> 8, its32.chunk & 0xff, its32.count, idx32it_getval(&its32));
        ++is32;
      }
      assert(is32);
    }
  }
}

void hsh64test(Hsh1cont * cont)
{
  uint32_t n;
 
  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its64;
      uint32_t is64 = 0;
      for (its64 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its64); idx1it_increment(&its64))
      {
        uint64_t val64 = idx64it_getval(&its64);
        printf("  %d: it = { { %d, %d }, %d }, val = { %d, %d }\n",
          is64, its64.chunk >> 8, its64.chunk & 0xff, its64.count, (uint32_t)val64, (uint32_t)(val64 >> 32));
        ++is64;
      }
      assert(is64);
    }
  }
}

void hsh32ftest(Hsh1cont * cont)
{
  uint32_t n;

  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its64;
      uint32_t is64 = 0;
      for (its64 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its64); idx1it_increment(&its64))
      {
        uint64_t val64 = idx64it_getval(&its64);
        uint32_t val32 = val64 >> 32;
        float valf;
        memcpy(&valf, &val32, sizeof(float));
        printf("  %d: it = { { %d, %d }, %d }, val = { %d, %4.2f }\n",
          is64, its64.chunk >> 8, its64.chunk & 0xff, its64.count, (uint32_t)val64, valf);
        ++is64;
      }
      assert(is64);
    }
  }
}

void hsh96test(Hsh1cont * cont)
{
  uint32_t n;
 
  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its96;
      uint32_t is96 = 0;
      for (its96 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its96); idx1it_increment(&its96))
      {
        Idx1cont val96 = idx96it_getval(&its96);
        printf("  %d: it = { { %d, %d }, %d }, val = %d, { %d, %d }\n",
          is96, its96.chunk >> 8, its96.chunk & 0xff, its96.count,
          val96.base_idx, val96.end.chunk, val96.end.count);
        ++is96;
      }
      assert(is96);
    }
  }
}

void hsh32f2test(Hsh1cont * cont)
{
  uint32_t n;

  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its96;
      uint32_t is96 = 0;
      for (its96 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its96); idx1it_increment(&its96))
      {
        Idx1cont val96 = idx96it_getval(&its96);
        float val[2];
        memcpy(&val[0], &val96.end.chunk, sizeof(float));
        memcpy(&val[1], &val96.end.count, sizeof(float));
        printf("  %d: it = { { %d, %d }, %d }, val = %d, { %4.2f, %4.2f }\n",
          is96, its96.chunk >> 8, its96.chunk & 0xff, its96.count,
          val96.base_idx, val[0], val[1]);
        ++is96;
      }
      assert(is96);
    }
  }
}

void hsh128test(Hsh1cont * cont)
{
  uint32_t n;
 
  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its128;
      uint32_t is128 = 0;
      for (its128 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its128); idx1it_increment(&its128))
      {
        Idx1range val128 = idx128it_getval(&its128);
        printf("  %d: it = { { %d, %d }, %d }, val = { %d, %d }, { %d, %d }\n",
          is128, its128.chunk >> 8, its128.chunk & 0xff, its128.count,
          val128.begin.chunk, val128.begin.count, val128.end.chunk, val128.end.count);
        ++is128;
      }
      assert(is128);
    }
  }
}

void hsh160test(Hsh1cont * cont)
{
  uint32_t n;
 
  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its160;
      uint32_t is160 = 0;
      for (its160 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its160); idx1it_increment(&its160))
      {
        Idx1nonsense1 val160 = idx160it_getval(&its160);
        printf("  %d: it = { { %d, %d }, %d }, val = %d { %d, %d }, { %d, %d }\n",
          is160, its160.chunk >> 8, its160.chunk & 0xff, its160.count, val160.cont.base_idx,
          val160.cont.end.chunk, val160.cont.end.count, val160.end.chunk, val160.end.count);
        ++is160;
      }
      assert(is160);
    }
  }
}

void hsh192test(Hsh1cont * cont)
{
  uint32_t n;
 
  printf ("S = %d, L = %d, N = 2, bits = %d\n", cont->S, cont->L, idx1cont_get_bits(&cont->bucket[0]));
  for (n = 0; n < cont->num_buckets; ++n)
  {
    printf(" %d: [%d]\n", n, cont->bucket[n].base_idx & 0xffffff);
    if (cont->bucket[n].base_idx & 0xffffff)
    {
      Idx1it its192;
      uint32_t is192 = 0;
      for (its192 = idx1it_begin(&cont->bucket[n]); idx1it_valid(&its192); idx1it_increment(&its192))
      {
        Idx1nonsense2 val192 = idx192it_getval(&its192);
        printf("  %d: it = { { %d, %d }, %d }, val = { %d, %d }, { %d, %d } key = { %d, %d }\n",
          is192, its192.chunk >> 8, its192.chunk & 0xff, its192.count,
          val192.range.begin.chunk, val192.range.begin.count,
          val192.range.end.chunk, val192.range.end.count, val192.end.chunk, val192.end.count);
        ++is192;
      }
      assert(is192);
    }
  }
}

void hsh32set_test()
{
  Hsh1cont some = hsh32set_new();
  hsh32set_add(&some, 456);
  hsh32set_add(&some, 789);
  hsh32set_add(&some, 321);
  hsh32set_add(&some, 442);
  hsh32set_add(&some, 552);
  hsh32set_add(&some, 553);
  hsh32set_add(&some, 554);
  hsh32set_add(&some, 555);
  hsh32set_add(&some, 555);
  hsh32set_add(&some, 557);
  hsh32set_add(&some, 558);
  hsh32set_add(&some, 559);
  hsh32set_add(&some, 560);
  assert(hsh32set_has(&some, 560));
  hsh32set_del(&some, 560);
  assert(!hsh32set_has(&some, 560));
  hsh32test(&some);
  hsh1cont_release(&some);
}

void hsh64set_test()
{
  Hsh1cont some = hsh64set_new();
  hsh64set_add(&some, 456ULL);
  hsh64set_add(&some, 456ULL << 32);
  hsh64set_add(&some, 789ULL);
  hsh64set_add(&some, 789ULL << 32);
  hsh64set_add(&some, 321ULL);
  hsh64set_add(&some, 321ULL << 32);
  hsh64set_add(&some, 442ULL);
  hsh64set_add(&some, 442ULL << 32);
  hsh64set_add(&some, 552ULL);
  hsh64set_add(&some, 552ULL << 32);
  hsh64set_add(&some, 553ULL);
  hsh64set_add(&some, 554ULL << 32);
  hsh64set_add(&some, 554ULL);
  hsh64set_add(&some, 555ULL << 32);
  hsh64set_add(&some, 555ULL);
  hsh64set_add(&some, 555ULL << 32);
  hsh64set_add(&some, 557ULL);
  hsh64set_add(&some, 557ULL << 32);
  hsh64set_add(&some, 558ULL);
  hsh64set_add(&some, 558ULL << 32);
  hsh64set_add(&some, 559ULL);
  hsh64set_add(&some, 559ULL << 32);
  hsh64set_add(&some, 560ULL << 32);
  assert(hsh64set_has(&some, 560ULL << 32));
  hsh64set_del(&some, 560ULL << 32);
  assert(!hsh64set_has(&some, 560ULL << 32));
  hsh64test(&some);
  hsh1cont_release(&some);
}

void hsh32map32_test()
{
  Hsh1cont some = hsh32map32_new();
  uint32_t val0 = 0;
  uint32_t val1 = 0;
  hsh32map32_add(&some, 456, 4560);
  hsh32map32_add(&some, 789, 7890);
  hsh32map32_add(&some, 321, 3210);
  hsh32map32_add(&some, 442, 4420);
  hsh32map32_add(&some, 552, 5520);
  hsh32map32_add(&some, 553, 5530);
  hsh32map32_add(&some, 554, 5540);
  hsh32map32_add(&some, 555, 5550);
  hsh32map32_add(&some, 555, 6660);
  hsh32map32_add(&some, 557, 5570);
  hsh32map32_add(&some, 558, 5580);
  hsh32map32_add(&some, 559, 5590);
  hsh32map32_add(&some, 560, 5600);
  assert(hsh32map32_get(&some, 560, &val0));
  printf("560 val = %d\n", val0);
  val0 = 5601;
  hsh32map32_set(&some, 560, val0);
  assert(hsh32map32_get(&some, 560, &val1));
  printf("560 val = %d\n", val1);
  hsh32map32_del(&some, 560);
  assert(!hsh32map32_get(&some, 560, &val0));
  hsh64test(&some);
  hsh1cont_release(&some);
}

void hsh32map32f_test()
{
  Hsh1cont some = hsh32map32f_new();
  float val0 = 0;
  float val1 = 0;
  hsh32map32f_add(&some, 456, 4560.6f);
  hsh32map32f_add(&some, 789, 7890.9f);
  hsh32map32f_add(&some, 321, 3210.1f);
  hsh32map32f_add(&some, 442, 4420.2f);
  hsh32map32f_add(&some, 552, 5520.2f);
  hsh32map32f_add(&some, 553, 5530.3f);
  hsh32map32f_add(&some, 554, 5540.4f);
  hsh32map32f_add(&some, 555, 5550.5f);
  hsh32map32f_add(&some, 555, 6660.6f);
  hsh32map32f_add(&some, 557, 5570.7f);
  hsh32map32f_add(&some, 558, 5580.8f);
  hsh32map32f_add(&some, 559, 5590.9f);
  hsh32map32f_add(&some, 560, 5600.0f);
  assert(hsh32map32f_get(&some, 560, &val0));
  printf("560 val = %4.2f\n", val0);
  val0 = 5601.01f;
  hsh32map32f_set(&some, 560, val0);
  assert(hsh32map32f_get(&some, 560, &val1));
  printf("560 val = %4.2f\n", val1);
  hsh32map32f_del(&some, 560);
  assert(!hsh32map32f_get(&some, 560, &val0));
  hsh32ftest(&some);
  hsh1cont_release(&some);
}

void hsh64map32_test()
{
  Hsh1cont some = hsh64map32_new();
  uint32_t val0 = 0;
  uint32_t val1 = 0;
  hsh64map32_add(&some, 456ULL, 4560);
  hsh64map32_add(&some, 456ULL << 32, 4561);
  hsh64map32_add(&some, 789ULL, 7890);
  hsh64map32_add(&some, 789ULL << 32, 7891);
  hsh64map32_add(&some, 321ULL, 3210);
  hsh64map32_add(&some, 321ULL << 32, 3211);
  hsh64map32_add(&some, 442ULL, 4420);
  hsh64map32_add(&some, 442ULL << 32, 4421);
  hsh64map32_add(&some, 552ULL, 5520);
  hsh64map32_add(&some, 552ULL << 32, 5521);
  hsh64map32_add(&some, 553ULL, 5530);
  hsh64map32_add(&some, 554ULL << 32, 5541);
  hsh64map32_add(&some, 554ULL, 5540);
  hsh64map32_add(&some, 555ULL << 32, 5551);
  hsh64map32_add(&some, 555ULL, 5550);
  hsh64map32_add(&some, 555ULL << 32, 6661);
  hsh64map32_add(&some, 557ULL, 5570);
  hsh64map32_add(&some, 557ULL << 32, 5571);
  hsh64map32_add(&some, 558ULL, 5580);
  hsh64map32_add(&some, 558ULL << 32, 5581);
  hsh64map32_add(&some, 559ULL, 5590);
  hsh64map32_add(&some, 559ULL << 32, 5591);
  hsh64map32_add(&some, 560ULL << 32, 5601);
  assert(hsh64map32_get(&some, 560ULL << 32, &val0));
  printf("560 val = %d\n", val0);
  val0 = 5602;
  hsh64map32_set(&some, 560ULL << 32, val0);
  assert(hsh64map32_get(&some, 560ULL << 32, &val1));
  printf("560 val = %d\n", val1);
  hsh64map32_del(&some, 560ULL << 32);
  assert(!hsh64map32_get(&some, 560ULL << 32, &val0));
  hsh96test(&some);
  hsh1cont_release(&some);
}

void hsh32map64_test()
{
  Hsh1cont some = hsh32map64_new();
  uint64_t val0 = 0;
  uint64_t val1 = 0;
  hsh32map64_add(&some, 456, 4560);
  hsh32map64_add(&some, 789, 7890);
  hsh32map64_add(&some, 321, 3210);
  hsh32map64_add(&some, 442, 4420);
  hsh32map64_add(&some, 552, 5520);
  hsh32map64_add(&some, 553, 5530);
  hsh32map64_add(&some, 554, 5540);
  hsh32map64_add(&some, 555, 5550);
  hsh32map64_add(&some, 555, 6660);
  hsh32map64_add(&some, 557, 5570);
  hsh32map64_add(&some, 558, 5580);
  hsh32map64_add(&some, 559, 5590);
  hsh32map64_add(&some, 560, 5600);
  assert(hsh32map64_get(&some, 560, &val0));
  printf("560 val = %lld\n", val0);
  val0 = 5602;
  hsh32map64_set(&some, 560, val0);
  assert(hsh32map64_get(&some, 560, &val1));
  printf("560 val = %lld\n", val1);
  hsh32map64_del(&some, 560);
  assert(!hsh32map64_get(&some, 560, &val0));
  hsh96test(&some);
  hsh1cont_release(&some);
}

void hsh32map32f2_test()
{
  Hsh1cont some = hsh32map32f2_new();
  float val0[2] = { 0.f, 0.f };
  float val1[2] = { 0.f, 0.f };
  hsh32map32f2_add(&some, 456, (float[2]){ 4560.6f, 4561.61f });
  hsh32map32f2_add(&some, 789, (float[2]){ 7890.9f, 7891.91f });
  hsh32map32f2_add(&some, 321, (float[2]){ 3210.1f, 3211.11f });
  hsh32map32f2_add(&some, 442, (float[2]){ 4420.2f, 4421.21f });
  hsh32map32f2_add(&some, 552, (float[2]){ 5520.2f, 5521.21f });
  hsh32map32f2_add(&some, 553, (float[2]){ 5530.3f, 5531.31f });
  hsh32map32f2_add(&some, 554, (float[2]){ 5540.4f, 5541.41f });
  hsh32map32f2_add(&some, 555, (float[2]){ 5550.5f, 5551.51f });
  hsh32map32f2_add(&some, 555, (float[2]){ 6660.6f, 6661.61f });
  hsh32map32f2_add(&some, 557, (float[2]){ 5570.7f, 5571.71f });
  hsh32map32f2_add(&some, 558, (float[2]){ 5580.8f, 5581.81f });
  hsh32map32f2_add(&some, 559, (float[2]){ 5590.9f, 5591.91f });
  hsh32map32f2_add(&some, 560, (float[2]){ 5600.0f, 5601.01f });
  assert(hsh32map32f2_get(&some, 560, &val0));
  printf("560 val = %4.2f - %4.2f\n", val0[0], val0[1]);
  val0[0] = 5601.01f;
  val0[1] = 5602.02f;
  hsh32map32f2_set(&some, 560, val0);
  assert(hsh32map32f2_get(&some, 560, &val1));
  printf("560 val = %4.2f - %4.2f\n", val1[0], val1[1]);
  hsh32map32f2_del(&some, 560);
  assert(!hsh32map32f2_get(&some, 560, &val0));
  hsh32f2test(&some);
  hsh1cont_release(&some);
}

void hsh64map64_test()
{
  Hsh1cont some = hsh64map64_new();
  uint64_t val0 = 0;
  uint64_t val1 = 0;
  hsh64map64_add(&some, 456ULL, 4560);
  hsh64map64_add(&some, 456ULL << 32, 4561);
  hsh64map64_add(&some, 789ULL, 7890);
  hsh64map64_add(&some, 789ULL << 32, 7891);
  hsh64map64_add(&some, 321ULL, 3210);
  hsh64map64_add(&some, 321ULL << 32, 3211);
  hsh64map64_add(&some, 442ULL, 4420);
  hsh64map64_add(&some, 442ULL << 32, 4421);
  hsh64map64_add(&some, 552ULL, 5520);
  hsh64map64_add(&some, 552ULL << 32, 5521);
  hsh64map64_add(&some, 553ULL, 5530);
  hsh64map64_add(&some, 554ULL << 32, 5541);
  hsh64map64_add(&some, 554ULL, 5540);
  hsh64map64_add(&some, 555ULL << 32, 5551);
  hsh64map64_add(&some, 555ULL, 5550);
  hsh64map64_add(&some, 555ULL << 32, 6661);
  hsh64map64_add(&some, 557ULL, 5570);
  hsh64map64_add(&some, 557ULL << 32, 5571);
  hsh64map64_add(&some, 558ULL, 5580);
  hsh64map64_add(&some, 558ULL << 32, 5581);
  hsh64map64_add(&some, 559ULL, 5590);
  hsh64map64_add(&some, 559ULL << 32, 5591);
  hsh64map64_add(&some, 560ULL << 32, 5601);
  assert(hsh64map64_get(&some, 560ULL << 32, &val0));
  printf("560 val = %lld\n", val0);
  val0 = 5602;
  hsh64map64_set(&some, 560ULL << 32, val0);
  assert(hsh64map64_get(&some, 560ULL << 32, &val1));
  printf("560 val = %lld\n", val1);
  hsh64map64_del(&some, 560ULL << 32);
  assert(!hsh64map64_get(&some, 560ULL << 32, &val0));
  hsh128test(&some);
  hsh1cont_release(&some);
}

void hsh32map96_test()
{
  Hsh1cont some = hsh32map96_new(false/*clean_values*/);
  Idx1cont val0 = idx1cont_zero();
  Idx1cont val1 = idx1cont_zero();
  hsh32map96_add(&some, 456, (Idx1cont){ .base_idx = 4560, .end = { .chunk = 45600, .count = 456000 } });
  hsh32map96_add(&some, 789, (Idx1cont){ .base_idx = 7890, .end = { .chunk = 78900, .count = 789000 } });
  hsh32map96_add(&some, 321, (Idx1cont){ .base_idx = 3210, .end = { .chunk = 32100, .count = 321000 } });
  hsh32map96_add(&some, 442, (Idx1cont){ .base_idx = 4420, .end = { .chunk = 44200, .count = 442000 } });
  hsh32map96_add(&some, 552, (Idx1cont){ .base_idx = 5520, .end = { .chunk = 55200, .count = 552000 } });
  hsh32map96_add(&some, 553, (Idx1cont){ .base_idx = 5530, .end = { .chunk = 55300, .count = 553000 } });
  hsh32map96_add(&some, 554, (Idx1cont){ .base_idx = 5540, .end = { .chunk = 55400, .count = 554000 } });
  hsh32map96_add(&some, 555, (Idx1cont){ .base_idx = 5550, .end = { .chunk = 55500, .count = 555000 } });
  hsh32map96_add(&some, 555, (Idx1cont){ .base_idx = 6660, .end = { .chunk = 66600, .count = 666000 } });
  hsh32map96_add(&some, 557, (Idx1cont){ .base_idx = 5570, .end = { .chunk = 55700, .count = 557000 } });
  hsh32map96_add(&some, 558, (Idx1cont){ .base_idx = 5580, .end = { .chunk = 55800, .count = 558000 } });
  hsh32map96_add(&some, 559, (Idx1cont){ .base_idx = 5590, .end = { .chunk = 55900, .count = 559000 } });
  hsh32map96_add(&some, 560, (Idx1cont){ .base_idx = 5600, .end = { .chunk = 56000, .count = 560000 } });
  assert(hsh32map96_get(&some, 560, &val0));
  printf("560 val = %d { %d, %d }\n", val0.base_idx, val0.end.chunk, val0.end.count);
  val0 = (Idx1cont){ .base_idx = 5602, .end = { .chunk = 56020, .count = 560200 } };
  hsh32map96_set(&some, 560, val0);
  assert(hsh32map96_get(&some, 560, &val1));
  printf("560 val = %d { %d, %d }\n", val1.base_idx, val1.end.chunk, val1.end.count);
  hsh32map96_del(&some, 560);
  assert(!hsh32map96_get(&some, 560, &val0));
  hsh128test(&some);
  hsh1cont_release(&some);
}

void hsh64map96_test()
{
  Hsh1cont some = hsh64map96_new(false/*clean_values*/);
  Idx1cont val0 = idx1cont_zero();
  Idx1cont val1 = idx1cont_zero();
  hsh64map96_add(&some, 456ULL, (Idx1cont){ .base_idx = 4560, .end = { .chunk = 45600, .count = 456000 } });
  hsh64map96_add(&some, 456ULL << 32, (Idx1cont){ .base_idx = 4561, .end = { .chunk = 456, .count = 56 } });
  hsh64map96_add(&some, 789ULL, (Idx1cont){ .base_idx = 7890, .end = { .chunk = 78900, .count = 789000 } });
  hsh64map96_add(&some, 789ULL << 32, (Idx1cont){ .base_idx = 7891, .end = { .chunk = 789, .count = 89 } });
  hsh64map96_add(&some, 321ULL, (Idx1cont){ .base_idx = 3210, .end = { .chunk = 32100, .count = 321000 } });
  hsh64map96_add(&some, 321ULL << 32, (Idx1cont){ .base_idx = 3211, .end = { .chunk = 321, .count = 21 } });
  hsh64map96_add(&some, 442ULL, (Idx1cont){ .base_idx = 4420, .end = { .chunk = 44200, .count = 442000 } });
  hsh64map96_add(&some, 442ULL << 32, (Idx1cont){ .base_idx = 4421, .end = { .chunk = 442, .count = 42 } });
  hsh64map96_add(&some, 552ULL, (Idx1cont){ .base_idx = 5520, .end = { .chunk = 55200, .count = 552000 } });
  hsh64map96_add(&some, 552ULL << 32, (Idx1cont){ .base_idx = 5521, .end = { .chunk = 552, .count = 52 } });
  hsh64map96_add(&some, 553ULL, (Idx1cont){ .base_idx = 5530, .end = { .chunk = 55300, .count = 553000 } });
  hsh64map96_add(&some, 554ULL << 32, (Idx1cont){ .base_idx = 5541, .end = { .chunk = 554, .count = 54 } });
  hsh64map96_add(&some, 554ULL, (Idx1cont){ .base_idx = 5540, .end = { .chunk = 55400, .count = 552000 } });
  hsh64map96_add(&some, 555ULL << 32, (Idx1cont){ .base_idx = 5551, .end = { .chunk = 555, .count = 55 } });
  hsh64map96_add(&some, 555ULL, (Idx1cont){ .base_idx = 5550, .end = { .chunk = 55500, .count = 555000 } });
  hsh64map96_add(&some, 555ULL << 32, (Idx1cont){ .base_idx = 6661, .end = { .chunk = 666, .count = 66 } });
  hsh64map96_add(&some, 557ULL, (Idx1cont){ .base_idx = 5570, .end = { .chunk = 55700, .count = 557000 } });
  hsh64map96_add(&some, 557ULL << 32, (Idx1cont){ .base_idx = 5571, .end = { .chunk = 557, .count = 57 } });
  hsh64map96_add(&some, 558ULL, (Idx1cont){ .base_idx = 5580, .end = { .chunk = 55800, .count = 558000 } });
  hsh64map96_add(&some, 558ULL << 32, (Idx1cont){ .base_idx = 5581, .end = { .chunk = 558, .count = 58 } });
  hsh64map96_add(&some, 559ULL, (Idx1cont){ .base_idx = 5590, .end = { .chunk = 55900, .count = 559000 } });
  hsh64map96_add(&some, 559ULL << 32, (Idx1cont){ .base_idx = 5591, .end = { .chunk = 559, .count = 59 } });
  hsh64map96_add(&some, 560ULL << 32, (Idx1cont){ .base_idx = 5601, .end = { .chunk = 560, .count = 60 } });
  assert(hsh64map96_get(&some, 560ULL << 32, &val0));
  printf("560 val = %d { %d, %d }\n", val0.base_idx, val0.end.chunk, val0.end.count);
  val0 = (Idx1cont){ .base_idx = 5602, .end = { .chunk = 56020, .count = 560200 } };
  hsh64map96_set(&some, 560ULL << 32, val0);
  assert(hsh64map96_get(&some, 560ULL << 32, &val1));
  printf("560 val = %d { %d, %d }\n", val1.base_idx, val1.end.chunk, val1.end.count);
  hsh64map96_del(&some, 560ULL << 32);
  assert(!hsh64map96_get(&some, 560ULL << 32, &val0));
  hsh160test(&some);
  hsh1cont_release(&some);
}

void hsh32map128_test()
{
  Hsh1cont some = hsh32map128_new(false/*clean_values*/);
  Idx1range val0 = idx1range_zero();
  Idx1range val1 = idx1range_zero();
  Idx1it it;
  it = (Idx1it){ .chunk = 45600, .count = 456000 };
  hsh32map128_add(&some, 456, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 78900, .count = 789000 };
  hsh32map128_add(&some, 789, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 32100, .count = 321000 };
  hsh32map128_add(&some, 321, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 44200, .count = 442000 };
  hsh32map128_add(&some, 442, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55200, .count = 552000 };
  hsh32map128_add(&some, 552, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55300, .count = 553000 };
  hsh32map128_add(&some, 553, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55400, .count = 554000 };
  hsh32map128_add(&some, 554, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55500, .count = 555000 };
  hsh32map128_add(&some, 555, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 66600, .count = 666000 };
  hsh32map128_add(&some, 555, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55700, .count = 557000 };
  hsh32map128_add(&some, 557, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55800, .count = 558000 };
  hsh32map128_add(&some, 558, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55900, .count = 559000 };
  hsh32map128_add(&some, 559, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 56000, .count = 560000 };
  hsh32map128_add(&some, 560, (Idx1range){ .begin = it, .end = it });
  assert(hsh32map128_get(&some, 560, &val0));
  printf("560 val = { %d, %d }, { %d, %d }\n",
    val0.begin.chunk, val0.end.chunk, val0.end.chunk, val0.end.count);
  it = (Idx1it){ .chunk = 56020, .count = 560200 };
  val0 = (Idx1range){ .begin = it, .end = it };
  hsh32map128_set(&some, 560, val0);
  assert(hsh32map128_get(&some, 560, &val1));
  printf("560 val = { %d, %d }, { %d, %d }\n",
    val1.begin.chunk, val1.end.chunk, val1.end.chunk, val1.end.count);
  hsh32map128_del(&some, 560);
  assert(!hsh32map128_get(&some, 560, &val0));
  hsh160test(&some);
  hsh1cont_release(&some);
}

void hsh64map128_test()
{
  Hsh1cont some = hsh64map128_new(false/*clean_values*/);
  Idx1range val0 = idx1range_zero();
  Idx1range val1 = idx1range_zero();
  Idx1it it;
  it = (Idx1it){ .chunk = 45600, .count = 456000 };
  hsh64map128_add(&some, 456, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 45610, .count = 456100 };
  hsh64map128_add(&some, 456ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 78900, .count = 789000 };
  hsh64map128_add(&some, 789, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 78910, .count = 789100 };
  hsh64map128_add(&some, 789ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 32100, .count = 321000 };
  hsh64map128_add(&some, 321, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 32110, .count = 321100 };
  hsh64map128_add(&some, 321ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 44200, .count = 442000 };
  hsh64map128_add(&some, 442, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 44210, .count = 442100 };
  hsh64map128_add(&some, 442ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55200, .count = 552000 };
  hsh64map128_add(&some, 552, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55210, .count = 552100 };
  hsh64map128_add(&some, 552ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55300, .count = 553000 };
  hsh64map128_add(&some, 553, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55410, .count = 554100 };
  hsh64map128_add(&some, 554ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55400, .count = 554000 };
  hsh64map128_add(&some, 554, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55510, .count = 555100 };
  hsh64map128_add(&some, 555ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55500, .count = 555000 };
  hsh64map128_add(&some, 555, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 66610, .count = 666100 };
  hsh64map128_add(&some, 555ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55700, .count = 557000 };
  hsh64map128_add(&some, 557, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55710, .count = 557100 };
  hsh64map128_add(&some, 557ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55800, .count = 558000 };
  hsh64map128_add(&some, 558, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55810, .count = 558100 };
  hsh64map128_add(&some, 558ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55900, .count = 559000 };
  hsh64map128_add(&some, 559, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 55910, .count = 559100 };
  hsh64map128_add(&some, 559ULL << 32, (Idx1range){ .begin = it, .end = it });
  it = (Idx1it){ .chunk = 56010, .count = 560100 };
  hsh64map128_add(&some, 560ULL << 32, (Idx1range){ .begin = it, .end = it });
  assert(hsh64map128_get(&some, 560ULL << 32, &val0));
  printf("560 val = { %d, %d }, { %d, %d }\n",
    val0.begin.chunk, val0.end.chunk, val0.end.chunk, val0.end.count);
  it = (Idx1it){ .chunk = 56020, .count = 560200 };
  val0 = (Idx1range){ .begin = it, .end = it };
  hsh64map128_set(&some, 560ULL << 32, val0);
  assert(hsh64map128_get(&some, 560ULL << 32, &val1));
  printf("560 val = { %d, %d }, { %d, %d }\n",
    val1.begin.chunk, val1.end.chunk, val1.end.chunk, val1.end.count);
  hsh64map128_del(&some, 560ULL << 32);
  assert(!hsh64map128_get(&some, 560ULL << 32, &val0));
  hsh192test(&some);
  hsh1cont_release(&some);
}

void pri32que32_test()
{
  Idx1it it;
  uint32_t pri;
  float fltpri;
  uint32_t val;
  Pri1que que = pri1que_new(Pri1ord_ascend);
#if 0
  pri32que32_push_back(&que,  2, 'A');
  pri32que32_push_back(&que,  3, 'P');
  pri32que32_push_back(&que,  1, 'E');
  pri32que32_push_back(&que,  0, 'H');
  pri32que32_push_back(&que,  8, 'O');
  pri32que32_push_back(&que, 10, 'N');
  pri32que32_push_back(&que,  9, 'W');
  pri32que32_push_back(&que,  7, 'D');
  pri32que32_push_back(&que,  5, 'F');
  pri32que32_push_back(&que,  4, 'I');
  pri32que32_push_back(&que,  6, 'Y');
  pri32que32_sort(&que);
  if (que.binheap.base_idx & 0xffffff)
  for (it = idx1it_begin(&que.binheap); idx1it_valid(&it); idx1it_increment(&it))
  {
    pri32que32_get_it(&que, &it, &pri, &val);
    printf("%02d: %c\n", pri, (char)val);
  }
  printf("\n");
#endif
#if 0
  pri32que32_insert(&que,  2, 'A');
  pri32que32_insert(&que,  3, 'P');
  pri32que32_insert(&que,  1, 'E');
  pri32que32_insert(&que,  0, 'H');
  pri32que32_insert(&que,  8, 'O');
  pri32que32_insert(&que, 10, 'N');
  pri32que32_insert(&que,  9, 'W');
  pri32que32_insert(&que,  7, 'D');
  pri32que32_insert(&que,  5, 'F');
  pri32que32_insert(&que,  4, 'I');
  pri32que32_insert(&que,  6, 'Y');
  if (que.binheap.base_idx & 0xffffff)
  do
  {
    pri32que32_front(&que, &pri, &val);
    printf("%02d: %c\n", pri, (char)val);
    pri32que32_pop_front(&que);
  }
  while (!pri1que_empty(&que));
#endif
//#if 0
  pri32fque32_insert(&que,  .2, 'A');
  pri32fque32_insert(&que,  .3, 'P');
  pri32fque32_insert(&que,  .1, 'E');
  pri32fque32_insert(&que,  .0, 'H');
  pri32fque32_insert(&que,  .8, 'O');
  pri32fque32_insert(&que, 1.0, 'N');
  pri32fque32_insert(&que,  .9, 'W');
  pri32fque32_insert(&que,  .7, 'D');
  pri32fque32_insert(&que,  .5, 'F');
  pri32fque32_insert(&que,  .4, 'I');
  pri32fque32_insert(&que,  .6, 'Y');
  if (que.binheap.base_idx & 0xffffff)
  do
  {
    pri32fque32_front(&que, &fltpri, &val);
    printf("%2.1f: %c\n", fltpri, (char)val);
    pri32que32_pop_front(&que);
  }
  while (!pri1que_empty(&que));
//#endif
  pri1que_release(&que);
}

void gph2test(Gph1cont * cont)
{
  uint32_t n;
  Vtx1it vit;
  Dfs1it dit;
  Hsh1cont color;
  printf ("Vertices:\n");
  for (n = 0; n < cont->adjlist.num_buckets; ++n)
  {
    if (cont->adjlist.bucket[n].base_idx & 0xffffff)
    {
      Idx1it it;
      for (it = idx1it_begin(&cont->adjlist.bucket[n]); idx1it_valid(&it); idx1it_increment(&it))
      {
        Idx1range rng = idx128it_getval(&it);
        Idx1cont arr = { .base_idx = rng.begin.count, .end = rng.end };
        Idx1it ar = idx1it_begin(&arr);
        uint32_t j = 0;
        printf("  %d: [%d] key = %c [%d]\n",
        n, cont->adjlist.bucket[n].base_idx & 0xffffff, (char)rng.begin.chunk, arr.base_idx & 0xffffff);
        for (ar = idx1it_begin(&arr); idx1it_valid(&ar); idx1it_increment(&ar))
        {
          uint64_t val64 = idx64it_getval(&ar);
          bool blocked = (val64 >> 32) & gph1vnoway_flag;
          if (!blocked)
            printf("    %d: vtx = %c, idx = %d\n", j, (char)val64, (val64 >> 32) & gph1vindex_mask);
          else
            printf("    %d: vtx = -%c, idx = %d\n", j, (char)val64, (val64 >> 32) & gph1vindex_mask);
          j++;
        }
      }
    }
  }
}

void gph2test_close(void * userstruct, uint32_t vtx)
{
  Gph1cont * cont = (Gph1cont *)userstruct;
//  gph1cont_del_dir1_vtx(cont, vtx);
//printf("    close %c\n", (char)vtx);
}

void gph2test_cycle(void * userstruct, uint32_t vtx)
{
  Gph1cont * cont = (Gph1cont *)userstruct;
printf("    cycle %c\n", (char)vtx);
}

void gph2test_suite()
{
  uint32_t n;
  Dfs1it dit;
  Bfs1it bit;
  Idx1it it;
  char vtx = 0;
  bool res;
  Hsh1cont color;
  Gph1cont gph3;
  Gph1cont gph2 = gph1cont_new();
  Idx1range range = idx1range_zero();

#if 0
  gph1cont_add_dir1(&gph2, 'A', 'B');
  gph1cont_add_dir1(&gph2, 'B', 'C');
  gph1cont_add_dir1(&gph2, 'C', 'A');
  gph2test(&gph2);
  gph1cont_del_dir2_vtx(&gph2, 'A');
  gph2test(&gph2);
  //gph1cont_del_dir2(&gph2, 'B', 'C');
  //gph2test(&gph2);
  gph1cont_clear(&gph2);
#endif

#if 0
  gph1cont_add_dir1(&gph2, 'A', 'B');
  gph1cont_add_dir1(&gph2, 'B', 'C');
  gph1cont_add_dir1(&gph2, 'C', 'D');
  gph1cont_add_dir1(&gph2, 'D', 'A');
  gph1cont_add_dir1(&gph2, 'C', 'A');
  gph1cont_add_dir1(&gph2, 'B', 'D');
  gph2test(&gph2);
  gph1cont_del_dir2_vtx(&gph2, 'D');
  gph2test(&gph2);
  gph1cont_clear(&gph2);
#endif

#if 0
  //topo-sort
  gph1cont_add_dir1(&gph2, '0', '3');
  gph1cont_add_dir1(&gph2, '1', '3');
  gph1cont_add_dir1(&gph2, '1', '4');
  gph1cont_add_dir1(&gph2, '2', '1');
  gph1cont_add_dir1(&gph2, '3', '5');
  gph1cont_add_dir1(&gph2, '4', '6');
  gph1cont_add_dir1(&gph2, '5', '6');
  gph2test(&gph2);
  vtx = '2';
#endif

#if 0
  //dfs directed no loops
  gph1cont_add_dir1(&gph2, 'A', 'B');
  gph1cont_add_dir1(&gph2, 'A', 'F');
  ///gph1cont_add_dir1(&gph2, 'H', 'A');
  gph1cont_add_dir1(&gph2, 'A', 'H');
  gph1cont_add_dir1(&gph2, 'B', 'C');
  gph1cont_add_dir1(&gph2, 'F', 'G');
  gph1cont_add_dir1(&gph2, 'C', 'D');
  gph1cont_add_dir1(&gph2, 'C', 'E');
  gph2test(&gph2);
  vtx = 'A';
#endif

#if 0
  //dfs undirected no loops
  gph1cont_add_dir2(&gph2, 'A', 'B');
  gph1cont_add_dir2(&gph2, 'A', 'F');
  gph1cont_add_dir2(&gph2, 'A', 'H');
  gph1cont_add_dir2(&gph2, 'B', 'C');
  gph1cont_add_dir2(&gph2, 'F', 'G');
  gph1cont_add_dir2(&gph2, 'C', 'D');
  gph1cont_add_dir2(&gph2, 'C', 'E');
  gph2test(&gph2);
  vtx = 'A';
#endif

#if 0
  //loop
  gph1cont_add_dir1(&gph2, 'A', 'B');
  gph1cont_add_dir1(&gph2, 'B', 'C');
  gph1cont_add_dir1(&gph2, 'C', 'A');
  gph2test(&gph2);
  vtx = 'A';
#endif

//#if 0
  //page78
  gph1cont_add_dir2(&gph2, 'A', 'B');
  gph1cont_add_dir2(&gph2, 'A', 'C');
  gph1cont_add_dir2(&gph2, 'B', 'G');
  gph1cont_add_dir2(&gph2, 'B', 'E');
  gph1cont_add_dir2(&gph2, 'G', 'E');
  gph1cont_add_dir2(&gph2, 'C', 'F');
  gph1cont_add_dir2(&gph2, 'F', 'H');
  gph1cont_add_dir2(&gph2, 'F', 'D');
  gph1cont_add_dir2(&gph2, 'D', 'G');
  gph2test(&gph2);
  vtx = 'D';
//#endif

///#if 0
  printf ("DFS vtx:\n");
  color = hsh32map32_new();
  n = 0;
  dit = dfs1it_begin_vtx(&gph2, vtx, &color);
  dit.close = gph2test_close;
  dit.cycle = gph2test_cycle;
  dit.userstruct = &gph2;
  for ( ; dfs1it_valid(&dit); dfs1it_increment(&dit))
  {
    printf("  %d: key = %c d%d [%d]\n",
      n, (char)dfs1it_getvtx(&dit), dit.adjstack.end.count - 1, dfs1it_adjlist(&dit).base_idx & 0xffffff);
    ++n;
  }
  hsh1cont_release(&color);
///#endif

///#if 0
  color = hsh32map32_new();
  printf ("BFS vtx:\n");
  n = 0;
  bit = bfs1it_begin_vtx(&gph2, vtx, &color);
  for ( ; bfs1it_valid(&bit); bfs1it_increment(&bit))
  {
    printf("  %d: key = %c [%d]\n",
      n, (char)bfs1it_getvtx(&bit), bfs1it_adjlist(&bit).base_idx & 0xffffff);
    ++n;
  }
  bfs1it_finish(&bit);
  hsh1cont_release(&color);
///#endif

  //gph3 = gph1cont_copy(&gph2);
  //gph2test(&gph3);
  //gph1cont_release(&gph3);

//#if 0
  res = gph1cont_topo_sort(&gph2, &range);
  printf ("topo-sort (%s):\n", (res ? "true" : "false"));
  n = 0;
  for (it = range.begin; idx1it_valid(&it); idx1it_increment(&it))
  {
    printf("  %d: key = %c\n", n, (char)idx32it_getval(&it));
    ++n;
  }
  idx1queue_release(&range);
//#endif

  gph1cont_release(&gph2);
}

int main()
{
  //idx32pool_init(64);
  //idx32test_suite();
  //idx64test_suite();
  //idx96test_suite();
  //idx128test_suite();
  //idx160test_suite();
  //idx192test_suite();

  //hsh32set_test();
  //hsh64set_test();
  //hsh32map32_test();
  //hsh32map32f_test();
  //hsh32map32f2_test();
  //hsh64map32_test();
  //hsh32map64_test();
  //hsh64map64_test();
  //hsh32map96_test();
  //hsh64map96_test();
  //hsh32map128_test();
  //hsh64map128_test();

  pri32que32_test();
  //gph2test_suite();

  //getc(stdin);
  return 0;
}
