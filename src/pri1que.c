
#include "pri1que.h"
#include "idx1internal.h"

#include <string.h>

//"generic" priority queue methods family

Pri1que pri1que_new(Pri1ord ord)
{
  return (Pri1que){ .binheap = idx1cont_zero(), .ascend = ord == Pri1ord_ascend, .fpnum = false };
}

void pri1que_release(Pri1que * que)
{
  idx1cont_release(&que->binheap);
}

bool pri1que_empty(const Pri1que * que)
{
  return idx1stack_empty(&que->binheap);
}

//Pri32que32 methods family

static bool pri32que32_swap(Idx1it * it0, Idx1it * it1)
{
  uint64_t val0 = idx64it_getval(it0);
  uint64_t val1 = idx64it_getval(it1);
  idx64it_setval(it0, val1);
  idx64it_setval(it1, val0);
}

typedef bool (*pri32que32_cmpfunc)(uint32_t val0, uint32_t val1);

static bool pri32fque32_lesser(uint32_t val0, uint32_t val1)
{
  return *((float*)&val0) < *((float*)&val1);
}

static bool pri32fque32_greater(uint32_t val0, uint32_t val1)
{
  return *((float*)&val0) > *((float*)&val1);
}

static bool pri32que32_lesser(uint32_t val0, uint32_t val1)
{
  return val0 < val1;
}

static bool pri32que32_greater(uint32_t val0, uint32_t val1)
{
  return val0 > val1;
}

static void pri32que32_heapify_up(Pri1que * que, Idx1it init, pri32que32_cmpfunc isord)
{
  while (init.count >= 0)
  {
    int32_t parent_count = (init.count - 1) / 2;
    if (parent_count >= 0 && parent_count != init.count)
    {
      Idx1it parent = idx64it_cont_idx(&que->binheap, parent_count);
      uint64_t curr_val = idx64it_getval(&init);
      uint64_t parent_val = idx64it_getval(&parent);
      if (!isord((uint32_t)parent_val, (uint32_t)curr_val))
      {
        idx64it_setval(&init, parent_val);
        idx64it_setval(&parent, curr_val);
        init = parent;
        continue;
      }
    }
    break;
  }
}

static void pri32que32_heapify_down(Pri1que * que, Idx1it init,
 int32_t max_count, pri32que32_cmpfunc isord)
{
  while (init.count < max_count)
  {
    Idx1it curr = init;
    int32_t left_count = init.count * 2 + 1;
    Idx1it left;
    if (left_count < max_count)
    {
      uint64_t curr_val, left_val;
      left = idx64it_cont_idx(&que->binheap, left_count);
      curr_val = idx64it_getval(&curr);
      left_val = idx64it_getval(&left);
      if (!isord((uint32_t)curr_val, (uint32_t)left_val))
        curr = left;
    }
    if (left_count + 1 < max_count)
    {
      uint64_t curr_val, right_val;
      Idx1it right = left;
      idx1it_increment(&right);
      curr_val = idx64it_getval(&curr);
      right_val = idx64it_getval(&right);
      if (!isord((uint32_t)curr_val, (uint32_t)right_val))
        curr = right;
    }
    if (curr.count == init.count)
      break;
    pri32que32_swap(&init, &curr);
    init = curr;
  }
}

void pri32que32_insert(Pri1que * que, uint32_t pri, uint32_t val)
{
  Idx1it last = que->binheap.end;
  uint64_t val64 = pri | ((uint64_t)val << 32);
  idx64cont_append(&que->binheap, val64);
  pri32que32_heapify_up(que, last,
    que->fpnum ? (que->ascend ? pri32fque32_lesser : pri32fque32_greater)
               : (que->ascend ? pri32que32_lesser : pri32que32_greater));
}

void pri32que32_front(const Pri1que * que, uint32_t * out_pri, uint32_t * out_copy)
{
  Idx1it begin = idx1it_begin(&que->binheap);
  uint64_t val64 = idx64it_getval(&begin);
  *out_pri = (uint32_t)val64;
  *out_copy = val64 >> 32;
}

void pri32que32_pop_front(Pri1que * que)
{
  Idx1it begin = idx1it_begin(&que->binheap);
  idx64cont_remove(&que->binheap, &begin);
  Idx1it last = que->binheap.end;
  idx1it_link2_decrement(&last);
  pri32que32_heapify_down(que, begin, last.count + 1,
    que->fpnum ? (que->ascend ? pri32fque32_lesser : pri32fque32_greater)
               : (que->ascend ? pri32que32_lesser : pri32que32_greater));
}

//Pri32fque32 methods family

void pri32fque32_insert(Pri1que * que, float pri, uint32_t val)
{
  que->fpnum = true;
  pri32que32_insert(que, *((uint32_t*)&pri), val);
}

void pri32fque32_front(const Pri1que * que, float * out_pri, uint32_t * out_copy)
{
  pri32que32_front(que, (uint32_t*)out_pri, out_copy);
}
