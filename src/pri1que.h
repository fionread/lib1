
#pragma once

#include "idx1pool.h"

typedef struct Pri1que_t
{
  Idx1cont binheap;
  bool ascend;
  bool fpnum;
}
Pri1que;

typedef enum Pri1ord_en
{
  Pri1ord_descend,
  Pri1ord_ascend
}
Pri1ord;

//create/delete "generic" priority queue
Pri1que pri1que_new(Pri1ord ord);
void pri1que_release(Pri1que * que);

//"generic" priority queue methods family
bool pri1que_empty(const Pri1que * que);

//Pri32que32 methods family
void pri32que32_insert(Pri1que * que, uint32_t pri, uint32_t val);
void pri32que32_front(const Pri1que * que, uint32_t * out_pri, uint32_t * out_copy);
void pri32que32_pop_front(Pri1que * que);

//Pri32fque32 methods family
void pri32fque32_insert(Pri1que * que, float pri, uint32_t val);
void pri32fque32_front(const Pri1que * que, float * out_pri, uint32_t * out_copy);
